/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.service;

import com.proyectocrm.dao.VActividadDAO;
import com.proyectocrm.dao.VClienteDAO;
import com.proyectocrm.dao.VEmpleadoDAO;
import com.proyectocrm.dao.VVariedadDAO;
import com.proyectocrm.entity.VActividad;
import com.proyectocrm.entity.VCliente;
import com.proyectocrm.entity.VClienteVendedor;
import com.proyectocrm.entity.VEmpleado;
import com.proyectocrm.entity.VRegistroActividad;
import com.proyectocrm.entity.VTipoActividad;
import com.proyectocrm.entity.VVariedad;
import com.proyectocrm.entity.VVariedadActividad;
import com.proyectocrm.entity.VVendedor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@Path("/actividad")
public class ActivdadService {

    @POST
    @Path("/tipoActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String tipoActividades(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            boolean estado;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            estado = (boolean) auxIngreso.getBoolean("estado");
            VActividadDAO aDAO = new VActividadDAO();
            List<VTipoActividad> resul = new ArrayList<>();
            resul = aDAO.listoTipoActividades();
            if (null != resul) {
                JSONArray tipo = new JSONArray();
                for (VTipoActividad r : resul) {
                    JSONObject c = new JSONObject();
                    c.put("idTipoActividad", r.getVTipoActividadId());
                    c.put("nombre", r.getNombre());
                    tipo.put(c);
                }
                resp.put("verificacion", true);
                resp.put("tipos", tipo);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/nuevaActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String nuevaActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            String titulo = "";
            String descripcion = "";
            String finicio = "";
            String ffin = "";
            int idTipoActividad;
            int idEmpleado;
            int idCliente;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            titulo = (String) auxIngreso.getString("titulo");
            descripcion = (String) auxIngreso.getString("descripcion");
            finicio = (String) auxIngreso.getString("finicio");
            ffin = (String) auxIngreso.getString("ffin");
            idTipoActividad = (int) auxIngreso.getInt("idTipoActividad");
            idEmpleado = (int) auxIngreso.getInt("idEmpleado");
            idCliente = (int) auxIngreso.getInt("idCliente");

            SimpleDateFormat fechaFormato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            //Cliente
            VCliente cliente = new VCliente();
            if (idCliente > 0) {
                VClienteDAO cDAO = new VClienteDAO();
                cliente = cDAO.infoClientePorId(idCliente);
            }
            VActividadDAO aDAO = new VActividadDAO();
            VEmpleadoDAO eDAO = new VEmpleadoDAO();
            VActividad act = new VActividad();
            VTipoActividad tip = new VTipoActividad();
            VVendedor ven = new VVendedor();

            ven = eDAO.BuscarIdVendedor(idEmpleado);
            tip = aDAO.buscarTipoActividades(idTipoActividad);
            act.setvVendedorId(ven);
            act.setVTipoActividadId(tip);
            act.setTitulo(titulo);
            act.setDescripcion(descripcion);
            act.setFinicio(fechaFormato.parse(finicio));
            act.setFfin(fechaFormato.parse(ffin));
            act.setEstado(Boolean.TRUE);
            act.setEtapa("AC");
            act.setDescargado(Boolean.FALSE);
            act.setCreado(new Date());
            if (idCliente > 0) {
                act.setvClienteId(cliente);
            }

            boolean resul = false;
            resul = aDAO.nuevaActividad(act);
            if (resul) {
                resp.put("verificacion", true);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/actividadSiguientesEmpleado")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String actividadSiguientesEmpleado(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

            int idVendedor;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVendedor = (int) auxIngreso.getInt("idVendedor");
            VActividadDAO aDAO = new VActividadDAO();
            List<VActividad> resul = new ArrayList<>();
            resul = aDAO.listaActividadesSiguientes(idVendedor);
            if (null != resul) {
                JSONArray acts = new JSONArray();
                for (VActividad r : resul) {
                    JSONObject c = new JSONObject();
                    c.put("idActividad", r.getVActividadId());
                    c.put("titulo", r.getTitulo());
                    c.put("descripcion", r.getDescripcion());
                    c.put("inicio", formato.format(r.getFinicio()));
                    c.put("fin", formato.format(r.getFfin()));
                    c.put("etapa", r.getEtapa());
                    c.put("idTipoActividad", r.getVTipoActividadId().getVTipoActividadId());
                    c.put("actividad", r.getVTipoActividadId().getNombre());
                    c.put("descargado", r.getDescargado());
                    c.put("clientenombre", r.getvClienteId() == null ? " " : r.getvClienteId().getNombre().substring(0, 5));

                    acts.put(c);
                }
                resp.put("verificacion", true);
                resp.put("actividades", acts);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/actividadPorId")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String actividadPorId(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            int idActividad;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            VActividadDAO aDAO = new VActividadDAO();
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);
            if (null != resul) {
                JSONObject c = new JSONObject();
                c.put("idActividad", resul.getVActividadId());
                c.put("titulo", resul.getTitulo());
                c.put("descripcion", resul.getDescripcion());
                c.put("inicio", formato.format(resul.getFinicio()));
                c.put("fin", formato.format(resul.getFfin()));
                c.put("etapa", resul.getEtapa());
                c.put("idTipoActividad", resul.getVTipoActividadId().getVTipoActividadId());
                c.put("actividad", resul.getVTipoActividadId().getNombre());
                c.put("idCliente", resul.getvClienteId() == null ? null : resul.getvClienteId().getVClienteId());
                c.put("descargado", resul.getDescargado());

                c.put("finicioreal", resul.getFinicioreal() == null ? null : formato.format(resul.getFinicioreal()));
                c.put("ffinreal", resul.getFfinreal() == null ? null : formato.format(resul.getFfinreal()));
                c.put("latinicio", resul.getIniciox());
                c.put("longinicio", resul.getInicioy());
                c.put("latfin", resul.getFinx());
                c.put("longfin", resul.getFiny());
                c.put("observacion", resul.getObservacion());
                c.put("contacto", resul.getContacto());
                c.put("cargo", resul.getCargo());
                c.put("tiempo", resul.getTiempototal());

                resp.put("verificacion", true);
                resp.put("actividad", c);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/inactivarActividadPorId")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String inactivarActividadPorId(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idActividad;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            VActividadDAO aDAO = new VActividadDAO();
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);
            if (null != resul) {
                resul.setEstado(Boolean.FALSE);
                resul.setEtapa("AN");
                resul.setActualizado(new Date());
                boolean ejecutado = aDAO.actualizarActividad(resul);
                if (ejecutado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/descargadoActividadPorId")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String descargadoActividadPorId(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idActividad;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            VActividadDAO aDAO = new VActividadDAO();
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);
            if (null != resul) {
                resul.setDescargado(Boolean.TRUE);
                resul.setActualizado(new Date());
                boolean ejecutado = aDAO.actualizarActividad(resul);
                if (ejecutado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/asignarClienteActividadId")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String asignarClienteActividadId(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idActividad;
            int idCliente;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            idCliente = (int) auxIngreso.getInt("idCliente");

            //Cliente
            VClienteDAO cDAO = new VClienteDAO();
            VClienteVendedor cliente = new VClienteVendedor();
            cliente = cDAO.infoClienteVendedor(idCliente);

            //Actividad
            VActividadDAO aDAO = new VActividadDAO();
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);

            if (null != resul) {
                resul.setvClienteId(cliente.getVClienteId());
                boolean ejecutado = aDAO.actualizarActividad(resul);
                if (ejecutado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/iniciarRegistroActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String iniciarRegistroActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            int idActividad;
            String latInicio;
            String longInicio;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            latInicio = (String) auxIngreso.getString("latinicio");
            longInicio = (String) auxIngreso.getString("longinicio");

            //Actividad
            VActividadDAO aDAO = new VActividadDAO();
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);
            if (null != resul) {
                resul.setActualizado(new Date());
                resul.setEtapa("IN");
                resul.setFinicioreal(new Date());
                resul.setIniciox(latInicio);
                resul.setInicioy(longInicio);
                boolean actualizado = aDAO.actualizarActividad(resul);
                if (actualizado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/finalizarRegistroActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String finalizarRegistroActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            int idActividad;
            String latFin;
            String longFin;
            String observacion;
            String contacto;
            String cargo;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            latFin = (String) auxIngreso.getString("latfin");
            longFin = (String) auxIngreso.getString("lonfin");
            observacion = (String) auxIngreso.getString("observacion");
            contacto = (String) auxIngreso.getString("contacto");
            cargo = (String) auxIngreso.getString("cargo");

            VActividadDAO aDAO = new VActividadDAO();

            //Actividad
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);

            if (null != resul) {
                resul.setActualizado(new Date());
                resul.setFfinreal(new Date());
                int minutos = (int) ((new Date().getTime() - resul.getFinicioreal().getTime()) / 60000);
                resul.setTiempototal(minutos);
                //latitud --> x
                //longitud --> y
                resul.setFinx(latFin);
                resul.setFiny(longFin);

                resul.setEtapa("FN");
                resul.setObservacion(observacion);
                resul.setContacto(contacto);
                resul.setCargo(cargo);

                boolean actualizado = aDAO.actualizarActividad(resul);

                if (actualizado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }

            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/registroActividadPorActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String registroActividadPorActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            int idActividad;

            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            VActividadDAO aDAO = new VActividadDAO();
            //Buscar Registro de Actividad Unico Activo
            VRegistroActividad regAct = new VRegistroActividad();
            regAct = aDAO.buscarRegistroActividad(idActividad);

            if (null != regAct) {
                resp.put("verificacion", true);
                JSONObject c = new JSONObject();
                c.put("idRegistroActividad", regAct.getVRegistroActividadId());
                c.put("finicio", formato.format(regAct.getFinicio()));
                c.put("ffin", regAct.getFfin() == null ? null : formato.format(regAct.getFfin()));
                c.put("latinicio", regAct.getIniciox());
                c.put("longinicio", regAct.getInicioy());
                c.put("latfin", regAct.getFinx());
                c.put("longfin", regAct.getFiny());
                c.put("observacion", regAct.getObservacion());
                c.put("tiempo", regAct.getTiempototal());
                resp.put("registro", c);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/actividadesVendedor")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String actividadesVendedor(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            int idVendedor;
            String desde;
            String hasta;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVendedor = (int) auxIngreso.getInt("idVendedor");
            desde = (String) auxIngreso.getString("desde");
            hasta = (String) auxIngreso.getString("hasta");
            VActividadDAO aDAO = new VActividadDAO();
            List<VActividad> resul = new ArrayList<>();
            resul = aDAO.listaActividadesVendedor(idVendedor, desde, hasta);
            if (null != resul) {
                JSONArray acts = new JSONArray();
                for (VActividad r : resul) {
                    JSONObject c = new JSONObject();
                    c.put("idActividad", r.getVActividadId());
                    c.put("titulo", r.getTitulo());
                    c.put("descripcion", r.getDescripcion());
                    c.put("inicio", formato.format(r.getFinicio()));
                    c.put("fin", formato.format(r.getFfin()));
                    c.put("etapa", r.getEtapa());
                    c.put("idTipoActividad", r.getVTipoActividadId().getVTipoActividadId());
                    c.put("actividad", r.getVTipoActividadId().getNombre());
                    c.put("cliente", r.getvClienteId() != null ? r.getvClienteId().getNombre() : null);

                    if (r.getEtapa().equals("IN") || r.getEtapa().equals("FN")) {
                        c.put("contieneRegistro", true);
                        JSONObject regActividaObjeto = new JSONObject();
                        regActividaObjeto.put("idRegistroActividad", r.getVActividadId());
                        regActividaObjeto.put("inicioreg", formato.format(r.getFinicioreal()));
                        regActividaObjeto.put("finreg", r.getFfinreal() == null ? null : formato.format(r.getFfinreal()));
                        regActividaObjeto.put("latinicio", r.getIniciox());
                        regActividaObjeto.put("longinicio", r.getInicioy());
                        regActividaObjeto.put("latfin", r.getFinx());
                        regActividaObjeto.put("longfin", r.getFiny());
                        regActividaObjeto.put("observacion", r.getObservacion());
                        regActividaObjeto.put("contacto", r.getContacto());
                        regActividaObjeto.put("cargo", r.getCargo());
                        regActividaObjeto.put("tiempo", r.getTiempototal());
                        c.put("registroActividad", regActividaObjeto);
                    } else {
                        c.put("contieneRegistro", false);
                    }
                    acts.put(c);
                }
                resp.put("verificacion", true);
                resp.put("actividades", acts);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/actividadSiguientesEmpleadoOffline")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String actividadSiguientesEmpleadoOffline(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

            int idVendedor;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVendedor = (int) auxIngreso.getInt("idVendedor");
            VActividadDAO aDAO = new VActividadDAO();
            List<VActividad> resul = new ArrayList<>();
            resul = aDAO.listaActividadesSiguientes(idVendedor);
            if (null != resul) {
                JSONArray acts = new JSONArray();
                for (VActividad r : resul) {
                    JSONObject c = new JSONObject();
                    c.put("idActividad", r.getVActividadId());
                    c.put("titulo", r.getTitulo());
                    c.put("descripcion", r.getDescripcion());
                    c.put("inicio", formato.format(r.getFinicio()));
                    c.put("fin", formato.format(r.getFfin()));
                    c.put("etapa", r.getEtapa());
                    c.put("idTipoActividad", r.getVTipoActividadId().getVTipoActividadId());
                    c.put("actividad", r.getVTipoActividadId().getNombre());
                    acts.put(c);
                }
                resp.put("verificacion", true);
                resp.put("actividades", acts);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/sincronizarRegistroActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String sincronizarRegistroActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            int idActividad;
            String latInicio;
            String longInicio;
            String inicio;
            String fin;
            String latFin;
            String longFin;
            String comentario;
            String contacto;
            String cargo;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");
            latInicio = (String) auxIngreso.getString("latinicio");
            longInicio = (String) auxIngreso.getString("longinicio");
            inicio = (String) auxIngreso.getString("inicio");
            fin = (String) auxIngreso.getString("fin");
            latFin = (String) auxIngreso.getString("latfin");
            longFin = (String) auxIngreso.getString("longfin");
            comentario = (String) auxIngreso.getString("comentario");
            contacto = (String) auxIngreso.getString("contacto");
            cargo = (String) auxIngreso.getString("cargo");

            //Actividad
            VActividadDAO aDAO = new VActividadDAO();
            VActividad resul = new VActividad();
            resul = aDAO.buscarActividadPorId(idActividad);
            if (null != resul) {

                resul.setFinicioreal(formato.parse(inicio));
                resul.setFfinreal(formato.parse(fin));
                resul.setIniciox(latInicio);
                resul.setInicioy(longInicio);
                resul.setFinx(latFin);
                resul.setFiny(longFin);
                resul.setObservacion(comentario);
                resul.setCargo(cargo);
                resul.setContacto(contacto);
                resul.setActualizado(new Date());
                int minutos = (int) ((resul.getFfinreal().getTime() - resul.getFinicioreal().getTime()) / 60000);
                resul.setTiempototal(minutos);
                resul.setEtapa("FN");
                boolean actualizado = aDAO.actualizarActividad(resul);

                if (actualizado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }

            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/diasConActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String diasConActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            int idVendedor;
            String desde;
            String hasta;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVendedor = (int) auxIngreso.getInt("idVendedor");
            desde = (String) auxIngreso.getString("desde");
            hasta = (String) auxIngreso.getString("hasta");
            VActividadDAO aDAO = new VActividadDAO();
            List<Object[]> resul = new ArrayList<>();
            resul = aDAO.diasConsulta(idVendedor, desde, hasta);
            if (null != resul) {
                JSONArray dias = new JSONArray();
                int i = 1;
                for (Object[] dia : resul) {
                    JSONObject d = new JSONObject();
                    d.put("fecha", dia[0]);
                    d.put("dia", dia[1]);
                    d.put("orden", i);
                    i++;
                    dias.put(d);
                }
                resp.put("verificacion", true);
                resp.put("dias", dias);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/nuevaVariedadEnActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String nuevaVariedadEnActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {

            int idVariedad;
            int idActividad;

            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVariedad = (int) auxIngreso.getInt("idVariedad");
            idActividad = (int) auxIngreso.getInt("idActividad");

            VVariedadDAO vDAO = new VVariedadDAO();
            VVariedad var = new VVariedad();
            var = vDAO.buscarVariedad(idVariedad);

            VActividadDAO aDAO = new VActividadDAO();
            VActividad act = new VActividad();
            act = aDAO.buscarActividadPorId(idActividad);

            if ((null != var)) {
                VVariedadActividad vVarAct = new VVariedadActividad();
                vVarAct.setVActividadId(act);
                vVarAct.setVVariedadId(var);
                vVarAct.setEstado(Boolean.TRUE);
                boolean reg = aDAO.nuevoVariedadActividad(vVarAct);
                if (reg) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/variedadesPorActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String variedadesPorActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idActividad;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idActividad = (int) auxIngreso.getInt("idActividad");

            VActividadDAO aDAO = new VActividadDAO();
            List<VVariedadActividad> resul = new ArrayList<>();
            resul = aDAO.variedadesPorActividad(idActividad);
            if (null != resul) {
                JSONArray variedades = new JSONArray();
                int i = 1;
                for (VVariedadActividad r : resul) {
                    JSONObject v = new JSONObject();
                    v.put("variedadactividadid", r.getVVariedadActividadId());
                    v.put("nombre", r.getVVariedadId().getNombre());
                    variedades.put(v);
                }
                resp.put("verificacion", true);
                resp.put("variedades", variedades);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/eliminarVariedadesPorActividad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String eliminarVariedadesPorActividad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int id;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            id = (int) auxIngreso.getInt("idVariedadActividad");

            VActividadDAO aDAO = new VActividadDAO();
            VVariedadActividad v = new VVariedadActividad();
            v = aDAO.variedadPorActividad(id);
            boolean resul;
            v.setEstado(Boolean.FALSE);
            resul = aDAO.eliminarVariedadActividad(v); // NO ELIMINA SOLO OCULTA
            if (resul) {
                resp.put("verificacion", true);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }
}
