/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.service;

import com.proyectocrm.dao.VClienteDAO;
import com.proyectocrm.dao.VEmpleadoDAO;
import com.proyectocrm.entity.VCliente;
import com.proyectocrm.entity.VClienteVendedor;
import com.proyectocrm.entity.VEmpleado;
import com.proyectocrm.entity.VVendedor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@Path("/cliente")
public class ClienteService {
    
    @POST
    @Path("/clientesVendedor")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String Factura(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            int idVendedor;
            int nivel;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVendedor= (Integer) auxIngreso.getInt("idVendedor");
            nivel= (Integer) auxIngreso.getInt("nivel");
            VClienteDAO cDAO= new VClienteDAO();
            List<VClienteVendedor> resul= new ArrayList<>();
            if(nivel==1){
                resul=cDAO.buscarTodosClientes();
            }else{
                resul=cDAO.buscarClientesPorVendedor(idVendedor);
            }
            
            
            if(null!=resul){
                JSONArray clientes= new JSONArray();
                for (VClienteVendedor r : resul) {
                JSONObject c= new JSONObject();
                c.put("idCliente", r.getVClienteId().getVClienteId());
                c.put("identificacion", r.getVClienteId().getIdentificacion());
                c.put("nombre", r.getVClienteId().getNombre());
                c.put("esNacional", r.getVClienteId().getEsNacional());
                c.put("telefono1", r.getVClienteId().getTelefono1());
                c.put("telefono2", r.getVClienteId().getTelefono2());
                c.put("correo", r.getVClienteId().getCorreo());
                c.put("direccion", r.getVClienteId().getDireccion());
                c.put("coordenadax", r.getVClienteId().getCoordenadax());
                c.put("coordenaday", r.getVClienteId().getCoordenaday());
                c.put("vendedor",r.getVVendedorId().getVEmpleadoId().getNombre());
                clientes.put(c);
                }               
                resp.put("verificacion", true);
                resp.put("clientes", clientes);
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        
        return resp.toString();
    }
    
    @POST
    @Path("/clientesPorNombre")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String clientesPorNombre(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            String nombre;
            
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            nombre= (String) auxIngreso.getString("nombre");
            
            VClienteDAO cDAO= new VClienteDAO();
            List <VCliente> resul= new ArrayList<>();
            
           resul= cDAO.buscarClientesPorNombre(nombre);
            
           if(null!=resul){
                JSONArray clientes= new JSONArray();
                for (VCliente r : resul) {
                JSONObject c= new JSONObject();
                c.put("idCliente", r.getVClienteId());
                c.put("identificacion", r.getIdentificacion());
                c.put("nombre", r.getNombre());
                c.put("esNacional", r.getEsNacional());
                c.put("telefono1", r.getTelefono1());
                c.put("telefono2", r.getTelefono2());
                c.put("correo", r.getCorreo());
                c.put("direccion", r.getDireccion());
                c.put("coordenadax", r.getCoordenadax());
                c.put("coordenaday", r.getCoordenaday());
                    try {
                        VClienteVendedor vcv= new VClienteVendedor();
                        vcv=cDAO.infoClienteVendedor(r.getVClienteId());
                        c.put("vendedor",vcv.getVVendedorId().getVEmpleadoId().getNombre());
                    } catch (Exception e) {
                        c.put("vendedor","Sin Asignar");
                    }
                clientes.put(c);
                }               
                resp.put("verificacion", true);
                resp.put("clientes", clientes);
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        
        return resp.toString();
    }
    
     @POST
    @Path("/clientesAdministrador")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String clientesAdministrador(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            int idVendedor;
            int nivel;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            nivel= (Integer) auxIngreso.getInt("nivel");
            VClienteDAO cDAO= new VClienteDAO();
            List<VCliente> resul= new ArrayList<>();
            resul=cDAO.todosClientesAdministrador(true);           
            if(null!=resul){
                JSONArray clientes= new JSONArray();
                for (VCliente r : resul) {
                JSONObject c= new JSONObject();
                c.put("idCliente", r.getVClienteId());
                c.put("identificacion", r.getIdentificacion());
                c.put("nombre", r.getNombre());
                c.put("esNacional", r.getEsNacional());
                c.put("telefono1", r.getTelefono1());
                c.put("telefono2", r.getTelefono2());
                c.put("correo", r.getCorreo());
                c.put("direccion", r.getDireccion());
                c.put("coordenadax", r.getCoordenadax());
                c.put("coordenaday", r.getCoordenaday());
                VClienteVendedor vendedorCliente= new VClienteVendedor();
                    try {
                        vendedorCliente=cDAO.infoClienteVendedor(r.getVClienteId());
                        c.put("vendedor",vendedorCliente.getVVendedorId().getVEmpleadoId().getNombre());
                    } catch (Exception e) {
                        c.put("vendedor","No Asignado");
                    }
                clientes.put(c);
                }               
                resp.put("verificacion", true);
                resp.put("clientes", clientes);
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        
        return resp.toString();
    }
    
    @POST
    @Path("/infocliente")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String infoCliente(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            int idCliente;
            String pwd="";
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idCliente= (Integer) auxIngreso.getInt("idCliente");
            VClienteDAO cDAO= new VClienteDAO();
            VCliente resul= new VCliente();
            resul=cDAO.infoClientePorId(idCliente);
            JSONObject c= new JSONObject();
            if(null!=resul){                
                c.put("idCliente", resul.getVClienteId());
                c.put("identificacion", resul.getIdentificacion());
                c.put("nombre", resul.getNombre());
                c.put("esNacional", resul.getEsNacional());
                c.put("telefono1", resul.getTelefono1());
                c.put("telefono2", resul.getTelefono2());
                c.put("correo", resul.getCorreo());
                c.put("direccion", resul.getDireccion());
                c.put("coordenadax", resul.getCoordenadax());
                c.put("coordenaday", resul.getCoordenaday());
                
                try {
                    VClienteVendedor resulVendedor= new VClienteVendedor();
                    resulVendedor=cDAO.infoClienteVendedor(idCliente);
                    c.put("vendedor", resulVendedor.getVVendedorId().getVEmpleadoId().getNombre());
                } catch (Exception e) {
                     c.put("vendedor", "No asignado");
                }
                
                resp.put("verificacion", true);
                resp.put("clientes", c);
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        
        return resp.toString();
    }
    
    
    @POST
    @Path("/nuevoCliente")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String nuevoCliente(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            String identificacion="";
            String nombre="";
            String telefono1="";
            String telefono2="";
            String email="";
            String direccion="";
            boolean esnacional;
            int empleadoid;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            identificacion = (String) auxIngreso.getString("identificacion");
            nombre = (String) auxIngreso.getString("nombre");
            telefono1 = (String) auxIngreso.getString("telefono1");
            telefono2 = (String) auxIngreso.getString("telefono2");
            email = (String) auxIngreso.getString("email");
            direccion = (String) auxIngreso.getString("direccion");
            esnacional = (boolean) auxIngreso.getBoolean("esnacional");
            empleadoid = (int) auxIngreso.getInt("empleadoid");
            
            VCliente nuevoCliente= new VCliente();
            nuevoCliente.setIdentificacion(identificacion);
            nuevoCliente.setNombre(nombre);
            nuevoCliente.setTelefono1(telefono1);
            nuevoCliente.setTelefono2(telefono2);
            nuevoCliente.setDireccion(direccion);
            nuevoCliente.setCorreo(email);
            nuevoCliente.setEsNacional(esnacional);
            nuevoCliente.setEstado(Boolean.TRUE);
            nuevoCliente.setCreadopor(empleadoid);
            nuevoCliente.setCreado(new Date());
            
            VClienteDAO cDAO= new VClienteDAO();
            boolean resul=cDAO.nuevoCliente(nuevoCliente);
            
            if(resul){                
                resp.put("verificacion", true);
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        
        return resp.toString();
    }
    
    @POST
    @Path("/asignarVendedorCliente")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String asignarVendedorCliente(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            int idVendedor;
            int idCliente;
            
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVendedor = (int) auxIngreso.getInt("idvendedor");
            idCliente = (int) auxIngreso.getInt("idcliente");
            
            VVendedor vendedor= new VVendedor();
            VEmpleadoDAO eDAO= new VEmpleadoDAO();
            vendedor=eDAO.vendedorId(idVendedor);
            
            VClienteDAO cDAO= new VClienteDAO();
            VClienteVendedor vcv =new VClienteVendedor();
            try {
                vcv= cDAO.infoClienteVendedor(idCliente);
                vcv.setVVendedorId(vendedor);
                boolean verifica=cDAO.actualizaClienteVendedor(vcv);
                if(verifica){
                    resp.put("verificacion", true);
                }else{
                    resp.put("verificacion", false);
                }
            } catch (Exception e) {
                VCliente cliente= new VCliente();
                cliente=cDAO.infoClientePorId(idCliente);
                vcv.setVClienteId(cliente);
                vcv.setVVendedorId(vendedor);
                boolean verifica=cDAO.nuevoClienteVendedor(vcv);
                if(verifica){
                    resp.put("verificacion", true);
                }else{
                    resp.put("verificacion", false);
                }   
            }   
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    
    @POST
    @Path("/editarCliente")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String editarCliente(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            int idCliente;
            String identificacion="";
            String nombre="";
            String telefono1="";
            String telefono2="";
            String email="";
            String direccion="";
            boolean esnacional;
            int empleadoid;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idCliente= (int) auxIngreso.getInt("idcliente");
            identificacion = (String) auxIngreso.getString("identificacion");
            nombre = (String) auxIngreso.getString("nombre");
            telefono1 = (String) auxIngreso.getString("telefono1");
            telefono2 = (String) auxIngreso.getString("telefono2");
            email = (String) auxIngreso.getString("email");
            direccion = (String) auxIngreso.getString("direccion");
            esnacional = (boolean) auxIngreso.getBoolean("esnacional");
            empleadoid = (int) auxIngreso.getInt("empleadoid");
            VClienteDAO cDAO= new VClienteDAO();
            VCliente cliente= new VCliente();
            
            cliente=cDAO.infoClientePorId(idCliente);
            
            cliente.setIdentificacion(identificacion);
            cliente.setNombre(nombre);
            cliente.setTelefono1(telefono1);
            cliente.setTelefono2(telefono2);
            cliente.setDireccion(direccion);
            cliente.setCorreo(email);
            cliente.setEsNacional(esnacional);
            cliente.setActualizado(new Date());
            cliente.setActualizadopor(empleadoid);
            boolean resul=cDAO.actualizaCliente(cliente);

            if(resul){                
                resp.put("verificacion", true);
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

}
