/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.service;

import com.proyectocrm.dao.VEmpleadoDAO;
import com.proyectocrm.entity.VEmpleado;
import com.proyectocrm.entity.VVendedor;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@Path("/empleado")
public class EmpleadoService {
    
    @POST
    @Path("/login")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String Factura(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            String user="";
            String pwd="";
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            user= (String) auxIngreso.getString("user");
            pwd= (String) auxIngreso.getString("password");
            
            VEmpleadoDAO eDAO= new VEmpleadoDAO();
            VEmpleado e= new VEmpleado();
            e=eDAO.validarUsuario(user, pwd);
            if(null!=e){
                resp.put("verificacion", true);
                JSONObject empleado= new JSONObject();
                empleado.put("v_empleado_id", e.getVEmpleadoId());
                empleado.put("nombre", e.getNombre());
                empleado.put("identificacion", e.getIdentificacion());
                empleado.put("estado", e.getEstado());
                empleado.put("nivel", e.getNivel());
                empleado.put("telefono", e.getTelefono());
                empleado.put("usuario", e.getUsuario());
                if(e.getNivel()==2){
                    VVendedor vendedor= new VVendedor();
                    try {
                        vendedor=eDAO.BuscarIdVendedor(e.getVEmpleadoId());
                        empleado.put("idVendedor", vendedor.getVVendedorId());
                    } catch (Exception ex) {
                    }
                }
                resp.put("datos", empleado);
                
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        
        return resp.toString();
    }
    
    @POST
    @Path("/infoEmpleado")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String infoEmpleado(String ingresoObjetoJson) throws JSONException{
        JSONObject resp = new JSONObject();
        try {
            int idEmpleado;
            
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idEmpleado= (int) auxIngreso.getInt("idEmpleado");
            
            VEmpleadoDAO eDAO= new VEmpleadoDAO();
            VEmpleado e= new VEmpleado();
            e=eDAO.buscarEmpleadoId(idEmpleado);
            if(null!=e){
                resp.put("verificacion", true);
                JSONObject empleado= new JSONObject();
                empleado.put("nombre", e.getNombre());
                empleado.put("identificacion", e.getIdentificacion());
                empleado.put("estado", e.getEstado());
                empleado.put("nivel", e.getNivel()==2 ? "Ventas":"Administración");
                empleado.put("telefono", e.getTelefono());
                empleado.put("usuario", e.getUsuario());
                resp.put("datos", empleado);
                
            }else{
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }        
        return resp.toString();
    }
    
    @POST
    @Path("/listadoVendedores")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String listadoVendedores(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            boolean estado;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            estado = (boolean) auxIngreso.getBoolean("estado");
            VEmpleadoDAO eDAO= new VEmpleadoDAO();
            List<VVendedor> listVendedores= new ArrayList<>();
            listVendedores=eDAO.BuscarVendedores();
            if (null != listVendedores) {
                JSONArray vendedorArrayJson = new JSONArray();
                for (VVendedor ven : listVendedores) {
                    JSONObject v = new JSONObject();
                    v.put("idVendedor", ven.getVVendedorId());
                    v.put("idEmpleado", ven.getVEmpleadoId().getVEmpleadoId());
                    v.put("nombre", ven.getVEmpleadoId().getNombre());
                    v.put("identificacion",ven.getVEmpleadoId().getIdentificacion());
                    v.put("telefono",ven.getVEmpleadoId().getTelefono());
                    vendedorArrayJson.put(v);
                }
                resp.put("verificacion", true);
                resp.put("vendedores", vendedorArrayJson);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }
}
