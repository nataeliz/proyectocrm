/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.service;

import com.proyectocrm.dao.VVariedadDAO;
import com.proyectocrm.entity.VBreeder;
import com.proyectocrm.entity.VBreederVariedad;
import com.proyectocrm.entity.VImagen;
import com.proyectocrm.entity.VVariedad;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author user
 */
@Path("/variedad")
public class VariedadService {


    @POST
    @Path("/buscarVariedades")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String buscarVariedades(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            String nombre;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            nombre = (String) auxIngreso.getString("nombre");
            VVariedadDAO vDAO = new VVariedadDAO();
            List<VVariedad> resul = new ArrayList<>();
            resul = vDAO.buscaVariedades(nombre);
            if (null != resul) {
                JSONArray variedad = new JSONArray();
                for (VVariedad var : resul) {
                    JSONObject v = new JSONObject();
                    v.put("idVariedad", var.getVVariedadId());
                    v.put("nombre", var.getNombre());
                    v.put("estado", var.getEstado());
                    try {
                        VBreederVariedad b = new VBreederVariedad();
                        b = vDAO.listaVariedadesBreeder(var.getVVariedadId());
                        v.put("breeder", b.getVBreederId().getNombre());
                        v.put("idBreeder", b.getVBreederId().getVBreederId());
                    } catch (Exception e) {
                        v.put("breeder", "No Asignado");
                        v.put("idBreeder",0);
                    }                   
                    variedad.put(v);
                }
                resp.put("verificacion", true);
                resp.put("tipos", variedad);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/listaVariedades")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String tipoActividades(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            boolean estado;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            estado = (boolean) auxIngreso.getBoolean("estado");
            VVariedadDAO vDAO = new VVariedadDAO();
            List<VVariedad> resul = new ArrayList<>();
            resul = vDAO.listaVariedades();
            if (null != resul) {
                JSONArray variedad = new JSONArray();
                for (VVariedad var : resul) {
                    JSONObject v = new JSONObject();
                    v.put("idVariedad", var.getVVariedadId());
                    v.put("nombre", var.getNombre());
                    v.put("estado", var.getEstado());
                    try {
                        VBreederVariedad b = new VBreederVariedad();
                        b = vDAO.listaVariedadesBreeder(var.getVVariedadId());
                        v.put("breeder", b.getVBreederId().getNombre());
                        v.put("idBreeder", b.getVBreederId().getVBreederId());
                    } catch (Exception e) {
                        v.put("breeder", "No Asignado");
                        v.put("idBreeder",0);
                    }                   
                    variedad.put(v);
                }
                resp.put("verificacion", true);
                resp.put("tipos", variedad);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/infoVariedad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String infoVariedad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idVariedad;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVariedad = (int) auxIngreso.getInt("idVariedad");
            VVariedadDAO vDAO = new VVariedadDAO();
            VVariedad resul = new VVariedad();
            resul = vDAO.buscarVariedad(idVariedad);
            JSONObject v = new JSONObject();
            if (null != resul) {
                v.put("idVariedad", resul.getVVariedadId());
                v.put("nombre", resul.getNombre());
                v.put("estado", resul.getEstado());
                try {
                    VBreederVariedad breeder= new VBreederVariedad();
                    breeder= vDAO.listaVariedadesBreeder(idVariedad);
                    v.put("breeder", breeder.getVBreederId().getNombre());
                    v.put("idBreeder", breeder.getVBreederId().getVBreederId());
                } catch (Exception e) {
                     v.put("breeder", "No Asignado");
                    v.put("idBreeder", 0);
                }
                resp.put("verificacion", true);
                resp.put("variedad", v);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/obtenerImgsVariedad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String obtenerImgsVariedad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idVariedad;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVariedad = (int) auxIngreso.getInt("idVariedad");
            VVariedadDAO vDAO = new VVariedadDAO();
            List<VImagen> resul = new ArrayList<>();
            resul = vDAO.listarImagenes(idVariedad);
            if (null != resul) {
               JSONArray imagenes = new JSONArray();
                for (VImagen im : resul) {
                    JSONObject img = new JSONObject();
                    img.put("codigo", im.getCodigo());
                    imagenes.put(img);
                }
                resp.put("verificacion", true);
                resp.put("imgs", imagenes);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/agregarImagen")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String agregarImagen(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idVariedad;
            String codigoImg;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVariedad = (int) auxIngreso.getInt("idVariedad");
            codigoImg = (String) auxIngreso.getString("codigoImg");
            
            VVariedadDAO vDAO = new VVariedadDAO();
            VBreederVariedad var= new VBreederVariedad();
            var= vDAO.listaVariedadesBreeder(idVariedad);
            
            if (null != var) {
                VImagen img= new VImagen();
                img.setEstado(Boolean.TRUE);
                img.setCodigo(codigoImg);
                img.setVVariedadId(var.getVVariedadId());
                boolean ejecutado = vDAO.nuevoRegistroImagen(img);
                if (ejecutado) {
                    resp.put("verificacion", true);
                } else {
                    resp.put("verificacion", false);
                }
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            resp.put("verificacion", false);
        }
        return resp.toString();
    }
    
    @POST
    @Path("/listaBreeders")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String listaBreeders(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            boolean estado;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            estado = (boolean) auxIngreso.getBoolean("estado");
            VVariedadDAO vDAO = new VVariedadDAO();
            List<VBreeder> resul = new ArrayList<>();
            resul = vDAO.listaBreeders();
            if (null != resul) {
                JSONArray breeders = new JSONArray();
                for (VBreeder var : resul) {
                    JSONObject v = new JSONObject();
                    v.put("idBreeder", var.getVBreederId());
                    v.put("nombre", var.getNombre());
                    v.put("estado", var.getEstado());
                    breeders.put(v);
                }
                resp.put("verificacion", true);
                resp.put("breeders", breeders);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }
    
    @POST
    @Path("/nuevaVariedad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String nuevaVariedad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idBreeder;
            String nombre="";
            
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);

            nombre = (String) auxIngreso.getString("nombre");
            
            VVariedadDAO vDAO = new VVariedadDAO();

                VVariedad nueva= new VVariedad();
                nueva.setEstado(Boolean.TRUE);
                nueva.setNombre(nombre);
                vDAO.nuevaVariedad(nueva);
                resp.put("verificacion", true);

        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }

    @POST
    @Path("/asginarBreederVariedad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String asginarBreederVariedad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {
            int idBreederNuevo;
            int idVariedad;
            int idBreederAnt;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idBreederNuevo = (int) auxIngreso.getInt("idBreederNuevo");
            idBreederAnt = (int) auxIngreso.getInt("idBreederAnt");
            idVariedad = (int) auxIngreso.getInt("idVariedad");
            
            VVariedadDAO vDAO = new VVariedadDAO();
            VVariedad var= new VVariedad();
            VBreeder breeder = new VBreeder();
            var= vDAO.buscarVariedad(idVariedad);
            breeder= vDAO.buscarBreeder(idBreederNuevo);
            
            if ((null != var) && (null!= breeder)) {
                if(idBreederAnt == 0 ){
                    VBreederVariedad nueva= new VBreederVariedad();
                    nueva.setVBreederId(breeder);
                    nueva.setVVariedadId(var);
                    vDAO.nuevaVariedadBreeder(nueva);
                    resp.put("verificacion", true);
                } else {
                    VBreederVariedad act= new VBreederVariedad();
                    act = vDAO.listaVariedadesBreeder(idVariedad);
                    act.setVBreederId(breeder);
                    vDAO.actualizaBreederVariedad(act);
                    resp.put("verificacion", true);
                }
                
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }
    
    @POST
    @Path("/editarVariedad")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String editarVariedad(String ingresoObjetoJson) throws JSONException {
        JSONObject resp = new JSONObject();
        try {

            int idVariedad;
            String nombre;
            JSONObject auxIngreso = new JSONObject(ingresoObjetoJson);
            idVariedad = (int) auxIngreso.getInt("idVariedad");
            nombre = (String) auxIngreso.getString("nombre");
            
            VVariedadDAO vDAO = new VVariedadDAO();
            VVariedad var= new VVariedad();

            var= vDAO.buscarVariedad(idVariedad);

            
            if ((null != var)) {
                var.setNombre(nombre);
                vDAO.actualizaVariedad(var);
                resp.put("verificacion", true);
            } else {
                resp.put("verificacion", false);
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
            resp.put("verificacion", false);
        }
        return resp.toString();
    }
    
    
    
}

