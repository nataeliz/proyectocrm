/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.utils;

import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
/**
 *
 * @author user
 */

@Named(value = "parametrosGenerales")
@ApplicationScoped
public class ParametrosGenerales {
     private static final Logger LOG = Logger.getLogger(ParametrosGenerales.class.getName());
     private Locale locale;
     private TimeZone timeZone;
     private String formatoFecha, local;
     
     /**
     * Creates a new instance of ParametrosGenerales
     */
    public ParametrosGenerales() {
        this.locale = new Locale("en", "US");
        this.timeZone = TimeZone.getTimeZone("GMT-5");
        this.formatoFecha = "dd/MM/yyyy";
        this.local = "es";

    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public String getFormatoFecha() {
        return formatoFecha;
    }

    public void setFormatoFecha(String formatoFecha) {
        this.formatoFecha = formatoFecha;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
    
    
    
}
