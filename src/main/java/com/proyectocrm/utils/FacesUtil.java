/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.utils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author user
 */
public class FacesUtil implements Serializable {

    /**
     * Obtiene el contexto del sistema.
     *
     * @return FacesContext
     */
    public static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     * Obtiene el ExternalContext del sistema.
     *
     * @return ExternalContext
     */
    public static ExternalContext getExternalContext() {
        return getFacesContext().getExternalContext();
    }

    /**
     *
     * @return
     */
    public static ServletContext getServletContext() {
        return (ServletContext) getExternalContext().getContext();
    }

    /**
     *
     * @param mensaje
     */
    public static void mensajeWarnDialog(final String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", mensaje);
        PrimeFaces.current().dialog().showMessageDynamic(msg);
    }

    /**
     * Obtiene los dos ultimos digitos del periodo.
     *
     * @param fecha
     * @return
     */
    public static String obtenerSinHoraFecha(Date fecha) {
        return new SimpleDateFormat("yyyy-MM-dd").format(fecha);
    }

    //Imagen
    public static String getPath() {
        try {
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            return ctx.getRealPath("/");
        } catch (Exception e) {
            addErrorMessage("getPath() " + e.getLocalizedMessage());
        }
        return null;
    }

    public static void addErrorMessage(String msg) {
        addErrorMessage(null, msg);
    }

    public static void addErrorMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    }

    public static void addCargarImagen() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Foto", "La foto se cargo correctamente"));
    }
    //Fin Imagen

    public static void addInfoMessageNew() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información registrada correctamente", "Los datos se han ingresado correctamente"));
    }

    public static void addErrorMessageNew() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, no se pudo registrar la información", "No se ingreso la información"));
    }

    public static void addInfoMessageEdit() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información modificado correctamente", "Los datos se han modificado correctamente"));
    }

    public static void addErrorMessageEdit() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, no se pudo modificar la información", "No se modifico la información"));
    }

    public static void addInfoMessageDel() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información eliminada correctamente", "Los datos se han eliminado correctamente"));
    }

    public static void addErrorMessageDel() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, no se puede eliminar la información", "No se eliminar la información"));
    }
}
