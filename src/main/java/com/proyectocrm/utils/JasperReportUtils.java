/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;



/**
 *
 * @author user
 */
@Named(value = "jasperReportUtil")
@Dependent
public class JasperReportUtils implements Serializable{
    
    private static final Logger LOG = Logger.getLogger(JasperReportUtils.class.getName());

//    @Resource(name = "plantaDS")
//    private DataSource plantaDS;
    private static final long serialVersionUID = -6363734689698747809L;

    public final static String PATH;
    public final static String PATHIMG;
    public final static String PATH_LOGO;
    public final static String TIPO_PDF;
    public final static String TIPO_DOCX;
    public final static String TIPO_XLS;
    public final static String TIPO_CSV;
    public final static String TIPO_HTML;

    public final static String PATH_ESTADO_ORDEN;
    public final static String PATH_SUB_REPORTE;
    public final static String PATH_ACTIVIDADES;
    public final static String PATH_ACTIVIDADES_EJECUTADO;
    public final static String PATH_ACTIVIDADES_DETALLADO;
    final ByteArrayOutputStream out = new ByteArrayOutputStream();

    static {
        PATH = FacesUtil.getServletContext().getRealPath("jrxml") + File.separator;
        PATHIMG = FacesUtil.getServletContext().getRealPath("resources") + File.separator + "img" + File.separator;
        PATH_LOGO = PATHIMG + "logo.jpg";
        PATH_ESTADO_ORDEN =PATH + File.separator + "bodega" + File.separator + "estadoProduccion.jasper";  
        PATH_SUB_REPORTE =PATH + File.separator; 
        PATH_ACTIVIDADES = PATH + File.separator + "rptActividades.jasper"; 
        PATH_ACTIVIDADES_EJECUTADO = PATH + File.separator + "rptActividadesEjecutado.jasper"; 
        PATH_ACTIVIDADES_DETALLADO = PATH + File.separator + "rptActividadesDetallado.jasper"; 
        TIPO_PDF = "application/pdf";
        TIPO_XLS = "application/vnd.ms-excel";
        TIPO_CSV = "application/csv";
        TIPO_HTML = "text/html";
        TIPO_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    }

    /**
     * Creates a new instance of JasperReportUtil
     */
    public JasperReportUtils() {
    }

    /**
     * Genera el reporte y lo visualiza en el browser.
     *
     * @param urlReporte Path del archivo jasper.
     * @param tipo Tipo de reporte, por el momento solo se generan pdf
     * @param nombrePersona Nombre del reporte
     * @param params Parámetros para el reporte
     * @throws ClassNotFoundException
     */
    public void generarReporteBD(final String urlReporte, String tipo,
            final String nombrePersona, Map<String, Object> params) throws ClassNotFoundException, NamingException, JRException {
        try {
            ExternalContext econtext = FacesUtil.getExternalContext();
            FacesContext fcontext = FacesUtil.getFacesContext();
            try (Connection conn = this.getCRMS().getConnection()) {
                Exporter exporter = null;

                InputStream inputStream = new FileInputStream(urlReporte);
                if (inputStream == null) {
                    throw new ClassNotFoundException("Archivo " + urlReporte + " no se encontró");
                }
                    JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, params, conn);
                jasperPrint.setProperty("net.sf.jasperreports.export.character.encoding", "UTF-8");
                HttpServletResponse response = (HttpServletResponse) econtext.getResponse();
                response.setContentType(tipo);
//                response.setHeader("Content-Disposition", "attachment; filename=\"reporte" + nombrePersona + ".pdf\";");
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
                if ("application/pdf".equals(tipo)) {
                    response.setHeader("Content-Disposition", "attachment; filename=\"reporte" + nombrePersona + ".pdf\";");
                    exporter = new JRPdfExporter();
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                }
                if ("text/html".equals(tipo)) {
                    response.setHeader("Content-Disposition", "attachment; filename=\"reporte" + nombrePersona + ".html\";");
                    exporter = new HtmlExporter();
                    exporter.setExporterOutput(new SimpleHtmlExporterOutput(response.getOutputStream()));
                    //exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    //JasperExportManager.exportReportToHtmlFile(jasperPrint, "C:/Users/user/Desktop/CRM/ProyectoCRM/src/main/webapp/jrxml/MyJasperReport.html");
                    
                    
//                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//                    exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
//                    exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
                    // System.out.println("Exportando a HTML");
                }
//                if ("application/rtf".equals(tipo)) {
//                    exporter = new JRRtfExporter();
//                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//                    // System.out.println("Exportando a RTF");
//                }
                if ("application/csv".equals(tipo)) {
                    exporter = new JRCsvExporter();
                    response.setHeader("Content-Disposition", "attachment; filename=\"reporte" + nombrePersona + ".csv\";");
                    SimpleCsvExporterConfiguration config = new SimpleCsvExporterConfiguration();
                    config.setFieldDelimiter("|");

                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
                    exporter.setConfiguration(config);
//                    // System.out.println("Exportando a CSV");
                }
                if ("application/vnd.ms-excel".equals(tipo)) {
                    response.setHeader("Content-Disposition", "attachment; filename=\"reporte" + nombrePersona + ".xls\";");
                    exporter = new JRXlsExporter();
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                }
                if (exporter != null) {
                    exporter.exportReport();
                }
            } catch (ClassNotFoundException | IOException ex) {
                LOG.log(Level.SEVERE, "Error al generar el reporte. ", ex);
            }
            fcontext.responseComplete();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Error al ejecutar la sentecia sql del reporte. ", ex);
        }
    }

    /**
     * Genera el reporte a partir de una colección de clases java.
     *
     * @param urlReporte Nombre del reporte y su url
     * @param tipo excel, pdf, etc
     * @param params Parámetros del reporte
     * @param dataSource Datos del reporte
     * @param nombre Nombre del reporte
     * @throws ClassNotFoundException
     */
    public void jasperReportBean(final String urlReporte, String tipo, Map<String, Object> params,
            JRDataSource dataSource, final String nombre) throws ClassNotFoundException {
//        try {
        ExternalContext econtext = FacesUtil.getExternalContext();
        FacesContext fcontext = FacesUtil.getFacesContext();
        try {
            Exporter exporter = null;

            InputStream inputStream = new FileInputStream(urlReporte);
            if (inputStream == null) {
                throw new ClassNotFoundException("Archivo " + urlReporte + " no se encontró");
            }
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, params, dataSource);
            HttpServletResponse response = (HttpServletResponse) econtext.getResponse();
            response.setContentType(tipo);
//            response.setHeader("Content-Disposition", "attachment; filename=\"actividades.pdf\";");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            if ("application/pdf".equals(tipo)) {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + ".pdf\";");
                exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
            }
//                if ("text/html".equals(tipo)) {
//                    exporter = new JR
//                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//                    exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
////                    exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
//                    // System.out.println("Exportando a HTML");
//                }
//                if ("application/rtf".equals(tipo)) {
//                    exporter = new JRRtfExporter();
//                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//                    // System.out.println("Exportando a RTF");
//                }
            if ("application/csv".equals(tipo)) {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + ".csv\";");
                exporter = new JRCsvExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
            }
            if ("application/vnd.ms-excel".equals(tipo)) {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + ".xls\";");
                exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
                configuration.setOnePagePerSheet(true);
                configuration.setCollapseRowSpan(false);
                configuration.setDetectCellType(true);
                configuration.setCellHidden(true);
                configuration.setFontSizeFixEnabled(false);
                configuration.setIgnoreAnchors(false);
                configuration.setIgnoreCellBackground(true);
                configuration.setIgnoreCellBorder(true);
                configuration.setShowGridLines(true);
                configuration.setFitWidth(250);
                exporter.setConfiguration(configuration);
            }
            if (exporter != null) {
                exporter.exportReport();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al generar el reporte. ", ex);
        }
        fcontext.responseComplete();
    }

    /**
     * Conexión con la base de datos.
     *
     * @return DataSource
     * @throws NamingException
     */
    private DataSource getCRMS() throws NamingException {
        Context c = new InitialContext();
//        return (DataSource) c.lookup("java:comp/env/plantaDS");
        return (DataSource) c.lookup("crmjdbc");
    }

}
