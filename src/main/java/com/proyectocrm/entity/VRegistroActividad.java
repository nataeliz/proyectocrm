/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_registro_actividad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VRegistroActividad.findAll", query = "SELECT v FROM VRegistroActividad v")
    , @NamedQuery(name = "VRegistroActividad.findByVRegistroActividadId", query = "SELECT v FROM VRegistroActividad v WHERE v.vRegistroActividadId = :vRegistroActividadId")
    , @NamedQuery(name = "VRegistroActividad.findByFinicio", query = "SELECT v FROM VRegistroActividad v WHERE v.finicio = :finicio")
    , @NamedQuery(name = "VRegistroActividad.findByFfin", query = "SELECT v FROM VRegistroActividad v WHERE v.ffin = :ffin")
    , @NamedQuery(name = "VRegistroActividad.findByIniciox", query = "SELECT v FROM VRegistroActividad v WHERE v.iniciox = :iniciox")
    , @NamedQuery(name = "VRegistroActividad.findByInicioy", query = "SELECT v FROM VRegistroActividad v WHERE v.inicioy = :inicioy")
    , @NamedQuery(name = "VRegistroActividad.findByFinx", query = "SELECT v FROM VRegistroActividad v WHERE v.finx = :finx")
    , @NamedQuery(name = "VRegistroActividad.findByFiny", query = "SELECT v FROM VRegistroActividad v WHERE v.finy = :finy")
    , @NamedQuery(name = "VRegistroActividad.findByEstado", query = "SELECT v FROM VRegistroActividad v WHERE v.estado = :estado")
    , @NamedQuery(name = "VRegistroActividad.findByObservacion", query = "SELECT v FROM VRegistroActividad v WHERE v.observacion = :observacion")})
public class VRegistroActividad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_registro_actividad_id")
    private Integer vRegistroActividadId;
    @Column(name = "finicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finicio;
    @Column(name = "ffin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ffin;
    @Size(max = 50)
    @Column(name = "iniciox")
    private String iniciox;
    @Size(max = 50)
    @Column(name = "inicioy")
    private String inicioy;
    @Size(max = 50)
    @Column(name = "finx")
    private String finx;
    @Size(max = 2147483647)
    @Column(name = "finy")
    private String finy;
    @Column(name = "estado")
    private Boolean estado;
    @Size(max = 1000)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "tiempototal")
    private Integer tiempototal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vRegistroActividadId")
    private List<VRecordatorio> vRecordatorioList;
    @JoinColumn(name = "v_actividad_id", referencedColumnName = "v_actividad_id")
    @ManyToOne(optional = false)
    private VActividad vActividadId;

    public VRegistroActividad() {
    }

    public VRegistroActividad(Integer vRegistroActividadId) {
        this.vRegistroActividadId = vRegistroActividadId;
    }

    public Integer getVRegistroActividadId() {
        return vRegistroActividadId;
    }

    public void setVRegistroActividadId(Integer vRegistroActividadId) {
        this.vRegistroActividadId = vRegistroActividadId;
    }

    public Date getFinicio() {
        return finicio;
    }

    public void setFinicio(Date finicio) {
        this.finicio = finicio;
    }

    public Date getFfin() {
        return ffin;
    }

    public void setFfin(Date ffin) {
        this.ffin = ffin;
    }

    public String getIniciox() {
        return iniciox;
    }

    public void setIniciox(String iniciox) {
        this.iniciox = iniciox;
    }

    public String getInicioy() {
        return inicioy;
    }

    public void setInicioy(String inicioy) {
        this.inicioy = inicioy;
    }

    public String getFinx() {
        return finx;
    }

    public void setFinx(String finx) {
        this.finx = finx;
    }

    public String getFiny() {
        return finy;
    }

    public void setFiny(String finy) {
        this.finy = finy;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @XmlTransient
    @JsonIgnore
    public List<VRecordatorio> getVRecordatorioList() {
        return vRecordatorioList;
    }

    public void setVRecordatorioList(List<VRecordatorio> vRecordatorioList) {
        this.vRecordatorioList = vRecordatorioList;
    }

    public VActividad getVActividadId() {
        return vActividadId;
    }

    public void setVActividadId(VActividad vActividadId) {
        this.vActividadId = vActividadId;
    }

    public Integer getTiempototal() {
        return tiempototal;
    }

    public void setTiempototal(Integer tiempototal) {
        this.tiempototal = tiempototal;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vRegistroActividadId != null ? vRegistroActividadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VRegistroActividad)) {
            return false;
        }
        VRegistroActividad other = (VRegistroActividad) object;
        if ((this.vRegistroActividadId == null && other.vRegistroActividadId != null) || (this.vRegistroActividadId != null && !this.vRegistroActividadId.equals(other.vRegistroActividadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VRegistroActividad[ vRegistroActividadId=" + vRegistroActividadId + " ]";
    }
    
}
