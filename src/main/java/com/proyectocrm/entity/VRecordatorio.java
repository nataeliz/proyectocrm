/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_recordatorio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VRecordatorio.findAll", query = "SELECT v FROM VRecordatorio v")
    , @NamedQuery(name = "VRecordatorio.findByVRecordatorioId", query = "SELECT v FROM VRecordatorio v WHERE v.vRecordatorioId = :vRecordatorioId")
    , @NamedQuery(name = "VRecordatorio.findByTitulo", query = "SELECT v FROM VRecordatorio v WHERE v.titulo = :titulo")
    , @NamedQuery(name = "VRecordatorio.findByFecha", query = "SELECT v FROM VRecordatorio v WHERE v.fecha = :fecha")})
public class VRecordatorio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_recordatorio_id")
    private Integer vRecordatorioId;
    @Size(max = 100)
    @Column(name = "titulo")
    private String titulo;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "v_registro_actividad_id", referencedColumnName = "v_registro_actividad_id")
    @ManyToOne(optional = false)
    private VRegistroActividad vRegistroActividadId;

    public VRecordatorio() {
    }

    public VRecordatorio(Integer vRecordatorioId) {
        this.vRecordatorioId = vRecordatorioId;
    }

    public Integer getVRecordatorioId() {
        return vRecordatorioId;
    }

    public void setVRecordatorioId(Integer vRecordatorioId) {
        this.vRecordatorioId = vRecordatorioId;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public VRegistroActividad getVRegistroActividadId() {
        return vRegistroActividadId;
    }

    public void setVRegistroActividadId(VRegistroActividad vRegistroActividadId) {
        this.vRegistroActividadId = vRegistroActividadId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vRecordatorioId != null ? vRecordatorioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VRecordatorio)) {
            return false;
        }
        VRecordatorio other = (VRecordatorio) object;
        if ((this.vRecordatorioId == null && other.vRecordatorioId != null) || (this.vRecordatorioId != null && !this.vRecordatorioId.equals(other.vRecordatorioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VRecordatorio[ vRecordatorioId=" + vRecordatorioId + " ]";
    }
    
}
