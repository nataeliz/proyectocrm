/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_tipo_actividad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VTipoActividad.findAll", query = "SELECT v FROM VTipoActividad v")
    , @NamedQuery(name = "VTipoActividad.findByVTipoActividadId", query = "SELECT v FROM VTipoActividad v WHERE v.vTipoActividadId = :vTipoActividadId")
    , @NamedQuery(name = "VTipoActividad.findByNombre", query = "SELECT v FROM VTipoActividad v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VTipoActividad.findByEstado", query = "SELECT v FROM VTipoActividad v WHERE v.estado = :estado")})
public class VTipoActividad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_tipo_actividad_id")
    private Integer vTipoActividadId;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "estado")
    private Boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vTipoActividadId")
    private List<VActividad> vActividadList;

    public VTipoActividad() {
    }

    public VTipoActividad(Integer vTipoActividadId) {
        this.vTipoActividadId = vTipoActividadId;
    }

    public Integer getVTipoActividadId() {
        return vTipoActividadId;
    }

    public void setVTipoActividadId(Integer vTipoActividadId) {
        this.vTipoActividadId = vTipoActividadId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    @JsonIgnore
    public List<VActividad> getVActividadList() {
        return vActividadList;
    }

    public void setVActividadList(List<VActividad> vActividadList) {
        this.vActividadList = vActividadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vTipoActividadId != null ? vTipoActividadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VTipoActividad)) {
            return false;
        }
        VTipoActividad other = (VTipoActividad) object;
        if ((this.vTipoActividadId == null && other.vTipoActividadId != null) || (this.vTipoActividadId != null && !this.vTipoActividadId.equals(other.vTipoActividadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VTipoActividad[ vTipoActividadId=" + vTipoActividadId + " ]";
    }
    
}
