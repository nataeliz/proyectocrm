/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_breeder_variedad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VBreederVariedad.findAll", query = "SELECT v FROM VBreederVariedad v")
    , @NamedQuery(name = "VBreederVariedad.findByVBreederVariedadId", query = "SELECT v FROM VBreederVariedad v WHERE v.vBreederVariedadId = :vBreederVariedadId")})
public class VBreederVariedad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_breeder_variedad_id")
    private Integer vBreederVariedadId;
    @JoinColumn(name = "v_breeder_id", referencedColumnName = "v_breeder_id")
    @ManyToOne(optional = false)
    private VBreeder vBreederId;
    @JoinColumn(name = "v_variedad_id", referencedColumnName = "v_variedad_id")
    @ManyToOne(optional = false)
    private VVariedad vVariedadId;

    public VBreederVariedad() {
    }

    public VBreederVariedad(Integer vBreederVariedadId) {
        this.vBreederVariedadId = vBreederVariedadId;
    }

    public Integer getVBreederVariedadId() {
        return vBreederVariedadId;
    }

    public void setVBreederVariedadId(Integer vBreederVariedadId) {
        this.vBreederVariedadId = vBreederVariedadId;
    }

    public VBreeder getVBreederId() {
        return vBreederId;
    }

    public void setVBreederId(VBreeder vBreederId) {
        this.vBreederId = vBreederId;
    }

    public VVariedad getVVariedadId() {
        return vVariedadId;
    }

    public void setVVariedadId(VVariedad vVariedadId) {
        this.vVariedadId = vVariedadId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vBreederVariedadId != null ? vBreederVariedadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VBreederVariedad)) {
            return false;
        }
        VBreederVariedad other = (VBreederVariedad) object;
        if ((this.vBreederVariedadId == null && other.vBreederVariedadId != null) || (this.vBreederVariedadId != null && !this.vBreederVariedadId.equals(other.vBreederVariedadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VBreederVariedad[ vBreederVariedadId=" + vBreederVariedadId + " ]";
    }
    
}
