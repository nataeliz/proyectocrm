/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_actividad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VActividad.findAll", query = "SELECT v FROM VActividad v")
    , @NamedQuery(name = "VActividad.findByVActividadId", query = "SELECT v FROM VActividad v WHERE v.vActividadId = :vActividadId")
    , @NamedQuery(name = "VActividad.findByTitulo", query = "SELECT v FROM VActividad v WHERE v.titulo = :titulo")
    , @NamedQuery(name = "VActividad.findByDescripcion", query = "SELECT v FROM VActividad v WHERE v.descripcion = :descripcion")
    , @NamedQuery(name = "VActividad.findByEstado", query = "SELECT v FROM VActividad v WHERE v.estado = :estado")
    , @NamedQuery(name = "VActividad.findByFinicio", query = "SELECT v FROM VActividad v WHERE v.finicio = :finicio")
    , @NamedQuery(name = "VActividad.findByFfin", query = "SELECT v FROM VActividad v WHERE v.ffin = :ffin")})
public class VActividad implements Serializable {

    @OneToMany(mappedBy = "vActividadId")
    private List<VVariedadActividad> vVariedadActividadList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_actividad_id")
    private Integer vActividadId;
    @Size(max = 50)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 300)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "estado")
    private Boolean estado;
    @Column(name = "descargado")
    private Boolean descargado;
    @Column(name = "finicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finicio;
    @Column(name = "ffin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ffin;
    @JoinColumn(name = "v_tipo_actividad_id", referencedColumnName = "v_tipo_actividad_id")
    @ManyToOne(optional = false)
    private VTipoActividad vTipoActividadId;
    @JoinColumn(name = "v_vendedor_id", referencedColumnName = "v_vendedor_id")
    @ManyToOne(optional = false)
    private VVendedor vVendedorId;
    @JoinColumn(name = "v_cliente_id", referencedColumnName = "v_cliente_id")
    @ManyToOne(optional = false)
    private VCliente vClienteId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vActividadId")
    private List<VRegistroActividad> vRegistroActividadList;
    @Size(max = 50)
    @Column(name = "etapa")
    private String etapa;
    @Column(name = "finicioreal")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finicioreal;
    @Column(name = "ffinreal")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ffinreal;
    @Size(max = 50)
    @Column(name = "iniciox")
    private String iniciox;
    @Size(max = 50)
    @Column(name = "inicioy")
    private String inicioy;
    @Size(max = 50)
    @Column(name = "finx")
    private String finx;
    @Size(max = 2147483647)
    @Column(name = "finy")
    private String finy;
    @Size(max = 1000)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "contacto")
    private String contacto;
     @Column(name = "cargo")
    private String cargo;
    @Column(name = "tiempototal")
    private Integer tiempototal;
    @Column(name = "creado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creado;
    
    @Column(name = "actualizado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualizado;
    
    public VActividad() {
    }

    public VActividad(Integer vActividadId) {
        this.vActividadId = vActividadId;
    }

    public Integer getVActividadId() {
        return vActividadId;
    }

    public void setVActividadId(Integer vActividadId) {
        this.vActividadId = vActividadId;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Date getFinicio() {
        return finicio;
    }

    public void setFinicio(Date finicio) {
        this.finicio = finicio;
    }

    public Date getFfin() {
        return ffin;
    }

    public void setFfin(Date ffin) {
        this.ffin = ffin;
    }

    public VTipoActividad getVTipoActividadId() {
        return vTipoActividadId;
    }

    public void setVTipoActividadId(VTipoActividad vTipoActividadId) {
        this.vTipoActividadId = vTipoActividadId;
    }

    public VVendedor getvVendedorId() {
        return vVendedorId;
    }

    public void setvVendedorId(VVendedor vVendedorId) {
        this.vVendedorId = vVendedorId;
    }

    public String getEtapa() {
        return etapa;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    public VCliente getvClienteId() {
        return vClienteId;
    }

    public void setvClienteId(VCliente vClienteId) {
        this.vClienteId = vClienteId;
    }  

    public Boolean getDescargado() {
        return descargado;
    }

    public void setDescargado(Boolean descargado) {
        this.descargado = descargado;
    }

    public Date getFinicioreal() {
        return finicioreal;
    }

    public void setFinicioreal(Date finicioreal) {
        this.finicioreal = finicioreal;
    }

    public Date getFfinreal() {
        return ffinreal;
    }

    public void setFfinreal(Date ffinreal) {
        this.ffinreal = ffinreal;
    }

    public String getIniciox() {
        return iniciox;
    }

    public void setIniciox(String iniciox) {
        this.iniciox = iniciox;
    }

    public String getInicioy() {
        return inicioy;
    }

    public void setInicioy(String inicioy) {
        this.inicioy = inicioy;
    }

    public String getFinx() {
        return finx;
    }

    public void setFinx(String finx) {
        this.finx = finx;
    }

    public String getFiny() {
        return finy;
    }

    public void setFiny(String finy) {
        this.finy = finy;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Integer getTiempototal() {
        return tiempototal;
    }

    public void setTiempototal(Integer tiempototal) {
        this.tiempototal = tiempototal;
    }

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }
    
    
    
    
    
    

    @XmlTransient
    @JsonIgnore
    public List<VRegistroActividad> getVRegistroActividadList() {
        return vRegistroActividadList;
    }

    public void setVRegistroActividadList(List<VRegistroActividad> vRegistroActividadList) {
        this.vRegistroActividadList = vRegistroActividadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vActividadId != null ? vActividadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VActividad)) {
            return false;
        }
        VActividad other = (VActividad) object;
        if ((this.vActividadId == null && other.vActividadId != null) || (this.vActividadId != null && !this.vActividadId.equals(other.vActividadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VActividad[ vActividadId=" + vActividadId + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<VVariedadActividad> getVVariedadActividadList() {
        return vVariedadActividadList;
    }

    public void setVVariedadActividadList(List<VVariedadActividad> vVariedadActividadList) {
        this.vVariedadActividadList = vVariedadActividadList;
    }
    
}
