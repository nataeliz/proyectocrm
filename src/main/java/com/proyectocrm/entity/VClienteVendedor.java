/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_cliente_vendedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VClienteVendedor.findAll", query = "SELECT v FROM VClienteVendedor v")
    , @NamedQuery(name = "VClienteVendedor.findByVClienteVendedorId", query = "SELECT v FROM VClienteVendedor v WHERE v.vClienteVendedorId = :vClienteVendedorId")})
public class VClienteVendedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_cliente_vendedor_id")
    private Integer vClienteVendedorId;
    @JoinColumn(name = "v_cliente_id", referencedColumnName = "v_cliente_id")
    @ManyToOne(optional = false)
    private VCliente vClienteId;
    @JoinColumn(name = "v_vendedor_id", referencedColumnName = "v_vendedor_id")
    @ManyToOne(optional = false)
    private VVendedor vVendedorId;

    public VClienteVendedor() {
    }

    public VClienteVendedor(Integer vClienteVendedorId) {
        this.vClienteVendedorId = vClienteVendedorId;
    }

    public Integer getVClienteVendedorId() {
        return vClienteVendedorId;
    }

    public void setVClienteVendedorId(Integer vClienteVendedorId) {
        this.vClienteVendedorId = vClienteVendedorId;
    }

    public VCliente getVClienteId() {
        return vClienteId;
    }

    public void setVClienteId(VCliente vClienteId) {
        this.vClienteId = vClienteId;
    }

    public VVendedor getVVendedorId() {
        return vVendedorId;
    }

    public void setVVendedorId(VVendedor vVendedorId) {
        this.vVendedorId = vVendedorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vClienteVendedorId != null ? vClienteVendedorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VClienteVendedor)) {
            return false;
        }
        VClienteVendedor other = (VClienteVendedor) object;
        if ((this.vClienteVendedorId == null && other.vClienteVendedorId != null) || (this.vClienteVendedorId != null && !this.vClienteVendedorId.equals(other.vClienteVendedorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VClienteVendedor[ vClienteVendedorId=" + vClienteVendedorId + " ]";
    }
    
}
