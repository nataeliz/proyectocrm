/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Desarrollador 2
 */
@Entity
@Table(name = "v_ventana")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VVentana.findAll", query = "SELECT v FROM VVentana v")
    , @NamedQuery(name = "VVentana.findByVVentanaId", query = "SELECT v FROM VVentana v WHERE v.vVentanaId = :vVentanaId")
    , @NamedQuery(name = "VVentana.findByNombre", query = "SELECT v FROM VVentana v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VVentana.findByEstado", query = "SELECT v FROM VVentana v WHERE v.estado = :estado")})
public class VVentana implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_ventana_id")
    private Integer vVentanaId;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vVentanaId")
    private List<VMenu> vMenuList;

    public VVentana() {
    }

    public VVentana(Integer vVentanaId) {
        this.vVentanaId = vVentanaId;
    }

    public Integer getVVentanaId() {
        return vVentanaId;
    }

    public void setVVentanaId(Integer vVentanaId) {
        this.vVentanaId = vVentanaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @XmlTransient
    @JsonIgnore
    public List<VMenu> getVMenuList() {
        return vMenuList;
    }

    public void setVMenuList(List<VMenu> vMenuList) {
        this.vMenuList = vMenuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vVentanaId != null ? vVentanaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VVentana)) {
            return false;
        }
        VVentana other = (VVentana) object;
        if ((this.vVentanaId == null && other.vVentanaId != null) || (this.vVentanaId != null && !this.vVentanaId.equals(other.vVentanaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VVentana[ vVentanaId=" + vVentanaId + " ]";
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
}
