/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Desarrollador 2
 */
@Entity
@Table(name = "v_rol")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VRol.findAll", query = "SELECT v FROM VRol v")
    , @NamedQuery(name = "VRol.findByVRolId", query = "SELECT v FROM VRol v WHERE v.vRolId = :vRolId")
    , @NamedQuery(name = "VRol.findByNombre", query = "SELECT v FROM VRol v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VRol.findByEstado", query = "SELECT v FROM VRol v WHERE v.estado = :estado")})
public class VRol implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vRolId")
    private List<VEmpleado> vEmpleadoList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_rol_id")
    private Integer vRolId;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vRolId")
    private List<VMenu> vMenuList;

    public VRol() {
    }

    public VRol(Integer vRolId) {
        this.vRolId = vRolId;
    }

    public Integer getVRolId() {
        return vRolId;
    }

    public void setVRolId(Integer vRolId) {
        this.vRolId = vRolId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @XmlTransient
    @JsonIgnore
    public List<VMenu> getVMenuList() {
        return vMenuList;
    }

    public void setVMenuList(List<VMenu> vMenuList) {
        this.vMenuList = vMenuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vRolId != null ? vRolId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VRol)) {
            return false;
        }
        VRol other = (VRol) object;
        if ((this.vRolId == null && other.vRolId != null) || (this.vRolId != null && !this.vRolId.equals(other.vRolId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VRol[ vRolId=" + vRolId + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<VEmpleado> getVEmpleadoList() {
        return vEmpleadoList;
    }

    public void setVEmpleadoList(List<VEmpleado> vEmpleadoList) {
        this.vEmpleadoList = vEmpleadoList;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
}
