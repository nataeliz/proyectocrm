/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCliente.findAll", query = "SELECT v FROM VCliente v")
    , @NamedQuery(name = "VCliente.findByVClienteId", query = "SELECT v FROM VCliente v WHERE v.vClienteId = :vClienteId")
    , @NamedQuery(name = "VCliente.findByIdentificacion", query = "SELECT v FROM VCliente v WHERE v.identificacion = :identificacion")
    , @NamedQuery(name = "VCliente.findByNombre", query = "SELECT v FROM VCliente v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VCliente.findByEsNacional", query = "SELECT v FROM VCliente v WHERE v.esNacional = :esNacional")
    , @NamedQuery(name = "VCliente.findByEstado", query = "SELECT v FROM VCliente v WHERE v.estado = :estado")
    , @NamedQuery(name = "VCliente.findByTelefono1", query = "SELECT v FROM VCliente v WHERE v.telefono1 = :telefono1")
    , @NamedQuery(name = "VCliente.findByTelefono2", query = "SELECT v FROM VCliente v WHERE v.telefono2 = :telefono2")
    , @NamedQuery(name = "VCliente.findByCorreo", query = "SELECT v FROM VCliente v WHERE v.correo = :correo")
    , @NamedQuery(name = "VCliente.findByDireccion", query = "SELECT v FROM VCliente v WHERE v.direccion = :direccion")
    , @NamedQuery(name = "VCliente.findByCoordenadax", query = "SELECT v FROM VCliente v WHERE v.coordenadax = :coordenadax")
    , @NamedQuery(name = "VCliente.findByCoordenaday", query = "SELECT v FROM VCliente v WHERE v.coordenaday = :coordenaday")})
public class VCliente implements Serializable {

    @OneToMany(mappedBy = "vClienteId")
    private List<VActividad> vActividadList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_cliente_id")
    private Integer vClienteId;
    @Size(max = 20)
    @Column(name = "identificacion")
    private String identificacion;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "es_nacional")
    private Boolean esNacional;
    @Column(name = "estado")
    private Boolean estado;
    @Size(max = 20)
    @Column(name = "telefono1")
    private String telefono1;
    @Size(max = 20)
    @Column(name = "telefono2")
    private String telefono2;
    @Size(max = 100)
    @Column(name = "correo")
    private String correo;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 50)
    @Column(name = "coordenadax")
    private String coordenadax;
    @Size(max = 50)
    @Column(name = "coordenaday")
    private String coordenaday;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vClienteId")
    private List<VClienteVendedor> vClienteVendedorList;
    @Column(name = "creado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creado;
    
    @Column(name = "actualizado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualizado;
    
    @Column(name = "creadopor")
    private Integer creadopor;
    
    @Column(name = "actualizadopor")
    private Integer actualizadopor;
    
    

    public VCliente() {
    }

    public VCliente(Integer vClienteId) {
        this.vClienteId = vClienteId;
    }

    public Integer getVClienteId() {
        return vClienteId;
    }

    public void setVClienteId(Integer vClienteId) {
        this.vClienteId = vClienteId;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEsNacional() {
        return esNacional;
    }

    public void setEsNacional(Boolean esNacional) {
        this.esNacional = esNacional;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCoordenadax() {
        return coordenadax;
    }

    public void setCoordenadax(String coordenadax) {
        this.coordenadax = coordenadax;
    }

    public String getCoordenaday() {
        return coordenaday;
    }

    public void setCoordenaday(String coordenaday) {
        this.coordenaday = coordenaday;
    }

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }

    public Integer getCreadopor() {
        return creadopor;
    }

    public void setCreadopor(Integer creadopor) {
        this.creadopor = creadopor;
    }

    public Integer getActualizadopor() {
        return actualizadopor;
    }

    public void setActualizadopor(Integer actualizadopor) {
        this.actualizadopor = actualizadopor;
    }
    
    
    
    

    @XmlTransient
    @JsonIgnore
    public List<VClienteVendedor> getVClienteVendedorList() {
        return vClienteVendedorList;
    }

    public void setVClienteVendedorList(List<VClienteVendedor> vClienteVendedorList) {
        this.vClienteVendedorList = vClienteVendedorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vClienteId != null ? vClienteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VCliente)) {
            return false;
        }
        VCliente other = (VCliente) object;
        if ((this.vClienteId == null && other.vClienteId != null) || (this.vClienteId != null && !this.vClienteId.equals(other.vClienteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VCliente[ vClienteId=" + vClienteId + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<VActividad> getVActividadList() {
        return vActividadList;
    }

    public void setVActividadList(List<VActividad> vActividadList) {
        this.vActividadList = vActividadList;
    }
    
}
