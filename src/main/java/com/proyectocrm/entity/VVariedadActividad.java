/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_variedad_actividad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VVariedadActividad.findAll", query = "SELECT v FROM VVariedadActividad v")
    , @NamedQuery(name = "VVariedadActividad.findByVVariedadActividadId", query = "SELECT v FROM VVariedadActividad v WHERE v.vVariedadActividadId = :vVariedadActividadId")})
public class VVariedadActividad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_variedad_actividad_id")
    private Integer vVariedadActividadId;
    @JoinColumn(name = "v_actividad_id", referencedColumnName = "v_actividad_id")
    @ManyToOne
    private VActividad vActividadId;
    @JoinColumn(name = "v_variedad_id", referencedColumnName = "v_variedad_id")
    @ManyToOne
    private VVariedad vVariedadId;
    @Column(name = "estado")
    private Boolean estado;
    
    public VVariedadActividad() {
    }

    public VVariedadActividad(Integer vVariedadActividadId) {
        this.vVariedadActividadId = vVariedadActividadId;
    }

    public Integer getVVariedadActividadId() {
        return vVariedadActividadId;
    }

    public void setVVariedadActividadId(Integer vVariedadActividadId) {
        this.vVariedadActividadId = vVariedadActividadId;
    }

    public VActividad getVActividadId() {
        return vActividadId;
    }

    public void setVActividadId(VActividad vActividadId) {
        this.vActividadId = vActividadId;
    }

    public VVariedad getVVariedadId() {
        return vVariedadId;
    }

    public void setVVariedadId(VVariedad vVariedadId) {
        this.vVariedadId = vVariedadId;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vVariedadActividadId != null ? vVariedadActividadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VVariedadActividad)) {
            return false;
        }
        VVariedadActividad other = (VVariedadActividad) object;
        if ((this.vVariedadActividadId == null && other.vVariedadActividadId != null) || (this.vVariedadActividadId != null && !this.vVariedadActividadId.equals(other.vVariedadActividadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VVariedadActividad[ vVariedadActividadId=" + vVariedadActividadId + " ]";
    }
    
}
