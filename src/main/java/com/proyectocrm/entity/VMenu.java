/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Desarrollador 2
 */
@Entity
@Table(name = "v_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VMenu.findAll", query = "SELECT v FROM VMenu v")
    , @NamedQuery(name = "VMenu.findByVMenuId", query = "SELECT v FROM VMenu v WHERE v.vMenuId = :vMenuId")
    , @NamedQuery(name = "VMenu.findByEstado", query = "SELECT v FROM VMenu v WHERE v.estado = :estado")
    , @NamedQuery(name = "VMenu.findByUrl", query = "SELECT v FROM VMenu v WHERE v.url = :url")
    , @NamedQuery(name = "VMenu.findByOrden", query = "SELECT v FROM VMenu v WHERE v.orden = :orden")
    , @NamedQuery(name = "VMenu.findByTipo", query = "SELECT v FROM VMenu v WHERE v.tipo = :tipo")})
public class VMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_menu_id")
    private Integer vMenuId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @Size(max = 100)
    @Column(name = "url")
    private String url;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "orden")
    private Double orden;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "tipo")
    private String tipo;
    @OneToMany(mappedBy = "codigoSubmenu")
    private List<VMenu> vMenuList;
    @JoinColumn(name = "codigo_submenu", referencedColumnName = "v_menu_id")
    @ManyToOne
    private VMenu codigoSubmenu;
    @JoinColumn(name = "v_rol_id", referencedColumnName = "v_rol_id")
    @ManyToOne(optional = false)
    private VRol vRolId;
    @JoinColumn(name = "v_ventana_id", referencedColumnName = "v_ventana_id")
    @ManyToOne(optional = false)
    private VVentana vVentanaId;

    public VMenu() {
    }

    public VMenu(Integer vMenuId) {
        this.vMenuId = vMenuId;
    }

    public VMenu(Integer vMenuId, boolean estado, String tipo) {
        this.vMenuId = vMenuId;
        this.estado = estado;
        this.tipo = tipo;
    }

    public Integer getVMenuId() {
        return vMenuId;
    }

    public void setVMenuId(Integer vMenuId) {
        this.vMenuId = vMenuId;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getOrden() {
        return orden;
    }

    public void setOrden(Double orden) {
        this.orden = orden;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    @JsonIgnore
    public List<VMenu> getVMenuList() {
        return vMenuList;
    }

    public void setVMenuList(List<VMenu> vMenuList) {
        this.vMenuList = vMenuList;
    }

    public VMenu getCodigoSubmenu() {
        return codigoSubmenu;
    }

    public void setCodigoSubmenu(VMenu codigoSubmenu) {
        this.codigoSubmenu = codigoSubmenu;
    }

    public VRol getVRolId() {
        return vRolId;
    }

    public void setVRolId(VRol vRolId) {
        this.vRolId = vRolId;
    }

    public VVentana getVVentanaId() {
        return vVentanaId;
    }

    public void setVVentanaId(VVentana vVentanaId) {
        this.vVentanaId = vVentanaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vMenuId != null ? vMenuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VMenu)) {
            return false;
        }
        VMenu other = (VMenu) object;
        if ((this.vMenuId == null && other.vMenuId != null) || (this.vMenuId != null && !this.vMenuId.equals(other.vMenuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VMenu[ vMenuId=" + vMenuId + " ]";
    }
    
}
