/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Desarrollador 2
 */
@Entity
@Table(name = "v_imagen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VImagen.findAll", query = "SELECT v FROM VImagen v")
    , @NamedQuery(name = "VImagen.findByVImagenId", query = "SELECT v FROM VImagen v WHERE v.vImagenId = :vImagenId")
    , @NamedQuery(name = "VImagen.findByEstado", query = "SELECT v FROM VImagen v WHERE v.estado = :estado")
    , @NamedQuery(name = "VImagen.findByCodigo", query = "SELECT v FROM VImagen v WHERE v.codigo = :codigo")
    , @NamedQuery(name = "VImagen.findByRutaImagen", query = "SELECT v FROM VImagen v WHERE v.rutaImagen = :rutaImagen")})
public class VImagen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_imagen_id")
    private Integer vImagenId;
    @Column(name = "estado")
    private Boolean estado;
    @Size(max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 200)
    @Column(name = "ruta_imagen")
    private String rutaImagen;
    @JoinColumn(name = "v_variedad_id", referencedColumnName = "v_variedad_id")
    @ManyToOne(optional = false)
    private VVariedad vVariedadId;

    public VImagen() {
    }

    public VImagen(Integer vImagenId) {
        this.vImagenId = vImagenId;
    }

    public Integer getVImagenId() {
        return vImagenId;
    }

    public void setVImagenId(Integer vImagenId) {
        this.vImagenId = vImagenId;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    public VVariedad getVVariedadId() {
        return vVariedadId;
    }

    public void setVVariedadId(VVariedad vVariedadId) {
        this.vVariedadId = vVariedadId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vImagenId != null ? vImagenId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VImagen)) {
            return false;
        }
        VImagen other = (VImagen) object;
        if ((this.vImagenId == null && other.vImagenId != null) || (this.vImagenId != null && !this.vImagenId.equals(other.vImagenId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VImagen[ vImagenId=" + vImagenId + " ]";
    }
    
}
