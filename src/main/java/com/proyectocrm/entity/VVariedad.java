/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_variedad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VVariedad.findAll", query = "SELECT v FROM VVariedad v")
    , @NamedQuery(name = "VVariedad.findByVVariedadId", query = "SELECT v FROM VVariedad v WHERE v.vVariedadId = :vVariedadId")
    , @NamedQuery(name = "VVariedad.findByNombre", query = "SELECT v FROM VVariedad v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VVariedad.findByEstado", query = "SELECT v FROM VVariedad v WHERE v.estado = :estado")})
public class VVariedad implements Serializable {

    @OneToMany(mappedBy = "vVariedadId")
    private List<VVariedadActividad> vVariedadActividadList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_variedad_id")
    private Integer vVariedadId;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "estado")
    private Boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vVariedadId")
    private List<VImagen> vImagenList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vVariedadId")
    private List<VBreederVariedad> vBreederVariedadList;

    public VVariedad() {
    }

    public VVariedad(Integer vVariedadId) {
        this.vVariedadId = vVariedadId;
    }

    public Integer getVVariedadId() {
        return vVariedadId;
    }

    public void setVVariedadId(Integer vVariedadId) {
        this.vVariedadId = vVariedadId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    @JsonIgnore
    public List<VImagen> getVImagenList() {
        return vImagenList;
    }

    public void setVImagenList(List<VImagen> vImagenList) {
        this.vImagenList = vImagenList;
    }

    @XmlTransient
    @JsonIgnore
    public List<VBreederVariedad> getVBreederVariedadList() {
        return vBreederVariedadList;
    }

    public void setVBreederVariedadList(List<VBreederVariedad> vBreederVariedadList) {
        this.vBreederVariedadList = vBreederVariedadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vVariedadId != null ? vVariedadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VVariedad)) {
            return false;
        }
        VVariedad other = (VVariedad) object;
        if ((this.vVariedadId == null && other.vVariedadId != null) || (this.vVariedadId != null && !this.vVariedadId.equals(other.vVariedadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VVariedad[ vVariedadId=" + vVariedadId + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<VVariedadActividad> getVVariedadActividadList() {
        return vVariedadActividadList;
    }

    public void setVVariedadActividadList(List<VVariedadActividad> vVariedadActividadList) {
        this.vVariedadActividadList = vVariedadActividadList;
    }
    
}
