/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_vendedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VVendedor.findAll", query = "SELECT v FROM VVendedor v")
    , @NamedQuery(name = "VVendedor.findByVVendedorId", query = "SELECT v FROM VVendedor v WHERE v.vVendedorId = :vVendedorId")
    , @NamedQuery(name = "VVendedor.findByEstado", query = "SELECT v FROM VVendedor v WHERE v.estado = :estado")})
public class VVendedor implements Serializable {

    @OneToMany(mappedBy = "vVendedorId")
    private List<VActividad> vActividadList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_vendedor_id")
    private Integer vVendedorId;
    @Column(name = "estado")
    private Boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vVendedorId")
    private List<VClienteVendedor> vClienteVendedorList;
    @JoinColumn(name = "v_empleado_id", referencedColumnName = "v_empleado_id")
    @ManyToOne(optional = false)
    private VEmpleado vEmpleadoId;

    public VVendedor() {
    }

    public VVendedor(Integer vVendedorId) {
        this.vVendedorId = vVendedorId;
    }

    public Integer getVVendedorId() {
        return vVendedorId;
    }

    public void setVVendedorId(Integer vVendedorId) {
        this.vVendedorId = vVendedorId;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    @JsonIgnore
    public List<VClienteVendedor> getVClienteVendedorList() {
        return vClienteVendedorList;
    }

    public void setVClienteVendedorList(List<VClienteVendedor> vClienteVendedorList) {
        this.vClienteVendedorList = vClienteVendedorList;
    }

    public VEmpleado getVEmpleadoId() {
        return vEmpleadoId;
    }

    public void setVEmpleadoId(VEmpleado vEmpleadoId) {
        this.vEmpleadoId = vEmpleadoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vVendedorId != null ? vVendedorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VVendedor)) {
            return false;
        }
        VVendedor other = (VVendedor) object;
        if ((this.vVendedorId == null && other.vVendedorId != null) || (this.vVendedorId != null && !this.vVendedorId.equals(other.vVendedorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VVendedor[ vVendedorId=" + vVendedorId + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<VActividad> getVActividadList() {
        return vActividadList;
    }

    public void setVActividadList(List<VActividad> vActividadList) {
        this.vActividadList = vActividadList;
    }
    
}
