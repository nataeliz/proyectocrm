/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Desarrollador 2
 */
@Entity
@Table(name = "v_empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VEmpleado.findAll", query = "SELECT v FROM VEmpleado v")
    , @NamedQuery(name = "VEmpleado.findByVEmpleadoId", query = "SELECT v FROM VEmpleado v WHERE v.vEmpleadoId = :vEmpleadoId")
    , @NamedQuery(name = "VEmpleado.findByIdentificacion", query = "SELECT v FROM VEmpleado v WHERE v.identificacion = :identificacion")
    , @NamedQuery(name = "VEmpleado.findByNombre", query = "SELECT v FROM VEmpleado v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VEmpleado.findByEstado", query = "SELECT v FROM VEmpleado v WHERE v.estado = :estado")
    , @NamedQuery(name = "VEmpleado.findByNivel", query = "SELECT v FROM VEmpleado v WHERE v.nivel = :nivel")
    , @NamedQuery(name = "VEmpleado.findByTelefono", query = "SELECT v FROM VEmpleado v WHERE v.telefono = :telefono")
    , @NamedQuery(name = "VEmpleado.findByUsuario", query = "SELECT v FROM VEmpleado v WHERE v.usuario = :usuario")
    , @NamedQuery(name = "VEmpleado.findByPassword", query = "SELECT v FROM VEmpleado v WHERE v.password = :password")})
public class VEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_empleado_id")
    private Integer vEmpleadoId;
    @Size(max = 20)
    @Column(name = "identificacion")
    private String identificacion;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "estado")
    private Boolean estado;
    @Column(name = "nivel")
    private Integer nivel;
    @Size(max = 20)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 20)
    @Column(name = "usuario")
    private String usuario;
    @Size(max = 200)
    @Column(name = "password")
    private String password;
    @JoinColumn(name = "v_rol_id", referencedColumnName = "v_rol_id")
    @ManyToOne(optional = false)
    private VRol vRolId;

    public VEmpleado() {
    }

    public VEmpleado(Integer vEmpleadoId) {
        this.vEmpleadoId = vEmpleadoId;
    }

    public Integer getVEmpleadoId() {
        return vEmpleadoId;
    }

    public void setVEmpleadoId(Integer vEmpleadoId) {
        this.vEmpleadoId = vEmpleadoId;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public VRol getVRolId() {
        return vRolId;
    }

    public void setVRolId(VRol vRolId) {
        this.vRolId = vRolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vEmpleadoId != null ? vEmpleadoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VEmpleado)) {
            return false;
        }
        VEmpleado other = (VEmpleado) object;
        if ((this.vEmpleadoId == null && other.vEmpleadoId != null) || (this.vEmpleadoId != null && !this.vEmpleadoId.equals(other.vEmpleadoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VEmpleado[ vEmpleadoId=" + vEmpleadoId + " ]";
    }
    
}
