/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author user
 */
@Entity
@Table(name = "v_breeder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VBreeder.findAll", query = "SELECT v FROM VBreeder v")
    , @NamedQuery(name = "VBreeder.findByVBreederId", query = "SELECT v FROM VBreeder v WHERE v.vBreederId = :vBreederId")
    , @NamedQuery(name = "VBreeder.findByNombre", query = "SELECT v FROM VBreeder v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "VBreeder.findByEstado", query = "SELECT v FROM VBreeder v WHERE v.estado = :estado")})
public class VBreeder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "v_breeder_id")
    private Integer vBreederId;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "estado")
    private Boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vBreederId")
    private List<VBreederVariedad> vBreederVariedadList;

    public VBreeder() {
    }

    public VBreeder(Integer vBreederId) {
        this.vBreederId = vBreederId;
    }

    public Integer getVBreederId() {
        return vBreederId;
    }

    public void setVBreederId(Integer vBreederId) {
        this.vBreederId = vBreederId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    @JsonIgnore
    public List<VBreederVariedad> getVBreederVariedadList() {
        return vBreederVariedadList;
    }

    public void setVBreederVariedadList(List<VBreederVariedad> vBreederVariedadList) {
        this.vBreederVariedadList = vBreederVariedadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vBreederId != null ? vBreederId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VBreeder)) {
            return false;
        }
        VBreeder other = (VBreeder) object;
        if ((this.vBreederId == null && other.vBreederId != null) || (this.vBreederId != null && !this.vBreederId.equals(other.vBreederId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectocrm.entity.VBreeder[ vBreederId=" + vBreederId + " ]";
    }
    
}
