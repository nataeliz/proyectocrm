/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VVentana;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Desarrollador 2
 */
public class VVentanaDAO {

    private EntityManagerFactory emf;

    public VVentanaDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public List<VVentana> listarVentanasCbx(boolean estado) {
        List<VVentana> roles = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VVentana v WHERE v.estado=:estado ORDER BY v.nombre ASC";
        Query query = em.createQuery(sql);
        query.setParameter("estado", estado);
        roles = query.getResultList();
        return roles;
    }
}
