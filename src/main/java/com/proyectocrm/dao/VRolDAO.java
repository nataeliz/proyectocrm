/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VRol;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Desarrollador 2
 */
public class VRolDAO {

    private EntityManagerFactory emf;

    public VRolDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public List<VRol> listarRoles() {
        List<VRol> roles = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VRol u ORDER BY u.nombre ASC";
        Query query = em.createQuery(sql);
        roles = query.getResultList();
        return roles;
    }

    public List<VRol> listarRolesCbx(boolean estado) {
        List<VRol> roles = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VRol u WHERE u.estado=:estado ORDER BY u.nombre ASC";
        Query query = em.createQuery(sql);
        query.setParameter("estado", estado);
        roles = query.getResultList();
        return roles;
    }

}
