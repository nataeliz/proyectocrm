/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VEmpleado;
import com.proyectocrm.entity.VVendedor;
import com.proyectocrm.utils.EncriptarClave;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author user
 */
public class VEmpleadoDAO {

    private EntityManagerFactory emf;

    public VEmpleadoDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public VEmpleado validarUsuario(String usuario, String pwd) {
        VEmpleado empleado = null;
        String pass_val = null;

        /*Encriptar */
        EncriptarClave cl = new EncriptarClave();
        pass_val = EncriptarClave.cifrar_sha512(pwd);
        try {
            EntityManager em = emf.createEntityManager();
            String sql = "SELECT u FROM VEmpleado u WHERE u.usuario=:usuario and u.password=:password";
            Query query = em.createQuery(sql);
            query.setParameter("usuario", usuario);
            query.setParameter("password", pass_val);
            empleado = (VEmpleado) query.getSingleResult();

            if (empleado == null) {
                empleado = null;
            }
        } catch (Exception e) {
            empleado = null;
        }
        return empleado;
    }

    public VEmpleado buscarEmpleadoId(int empleadoId) {
        VEmpleado empleado = null;
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VEmpleado v WHERE v.vEmpleadoId = :vEmpleadoId";
        Query query = em.createQuery(sql);
        query.setParameter("vEmpleadoId", empleadoId);
        empleado = (VEmpleado) query.getSingleResult();
        return empleado;
    }

    public VVendedor BuscarIdVendedor(int idEmpleado) {
        VVendedor vendedor = null;
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VVendedor u WHERE u.vEmpleadoId.vEmpleadoId=:idEmpleado";
        Query query = em.createQuery(sql);
        query.setParameter("idEmpleado", idEmpleado);
        vendedor = (VVendedor) query.getSingleResult();
        return vendedor;
    }

    public List<VVendedor> BuscarVendedores() {
        List<VVendedor> vendedores = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VVendedor u WHERE u.estado=true";
        Query query = em.createQuery(sql);
        vendedores = query.getResultList();
        return vendedores;
    }

    public VVendedor vendedorId(int idVendedor) {
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VVendedor u WHERE u.vVendedorId=:idVendedor";
        Query query = em.createQuery(sql);
        query.setParameter("idVendedor", idVendedor);
        return (VVendedor) query.getSingleResult();
    }

    public List<VEmpleado> todosEmpleadosAdministrador(boolean estado) {
        List<VEmpleado> empleados = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VEmpleado u WHERE u.estado=:estado ORDER BY u.nombre ASC";
        Query query = em.createQuery(sql);
        query.setParameter("estado", estado);
        empleados = query.getResultList();
        return empleados;
    }

    public List<VEmpleado> listaEmpleados() {
        List<VEmpleado> listaEmpleados = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VEmpleado v";
        Query query = em.createQuery(sql);
        listaEmpleados = query.getResultList();
        em.close();
        return listaEmpleados;
    }

    public Boolean nuevoEmpleado(VEmpleado objE) {
        boolean resp = false;
        String pass_val = null;

        /*Encriptar */
        EncriptarClave cl = new EncriptarClave();
        pass_val = EncriptarClave.cifrar_sha512(objE.getPassword());
        objE.setPassword(pass_val);
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(objE);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean actualizaEmpleado(VEmpleado objE) {
        boolean resp = false;
        String pass_val = null;

        /*Encriptar */
        EncriptarClave cl = new EncriptarClave();
        pass_val = EncriptarClave.cifrar_sha512(objE.getPassword());
        objE.setPassword(pass_val);
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(objE);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean eliminaEmpleado(VEmpleado objE) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            VEmpleado objEE = em.merge(objE);
            em.remove(objEE);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }
}
