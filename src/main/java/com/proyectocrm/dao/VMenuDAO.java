/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VMenu;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Desarrollador 2
 */
public class VMenuDAO {

    private EntityManagerFactory emf;

    public VMenuDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public List<VMenu> listaMenusEstado(boolean estado) {
        List<VMenu> menu = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VMenu v WHERE v.estado=:estado ORDER BY v.orden, v.vVentanaId.nombre";
        Query query = em.createQuery(sql);
        query.setParameter("estado", estado);
        menu = query.getResultList();
        return menu;
    }

    public List<VMenu> listaMenus() {
        List<VMenu> menu = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VMenu v ORDER BY v.orden, v.vVentanaId.nombre";
        Query query = em.createQuery(sql);
        menu = query.getResultList();
        return menu;
    }

    public List<VMenu> listaSubMenusEstado(boolean estado, String codigoSubmenu) {
        List<VMenu> menu = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VMenu v WHERE v.estado=:estado and v.codigoSubmenu=:codigoSubmenu ORDER BY v.orden, v.vVentanaId.nombre";
        Query query = em.createQuery(sql);
        query.setParameter("estado", estado);
        query.setParameter("codigoSubmenu", codigoSubmenu);
        menu = query.getResultList();
        return menu;
    }

    public Boolean nuevoMenu(VMenu objM) {
        boolean resp = false;

        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(objM);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean actualizaMenu(VMenu objM) {
        boolean resp = false;

        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(objM);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean eliminaMenu(VMenu objM) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            VMenu objMM = em.merge(objM);
            em.remove(objMM);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

}
