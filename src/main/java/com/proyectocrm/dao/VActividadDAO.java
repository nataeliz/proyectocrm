/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VActividad;
import com.proyectocrm.entity.VRegistroActividad;
import com.proyectocrm.entity.VTipoActividad;
import com.proyectocrm.entity.VVariedadActividad;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author user
 */
public class VActividadDAO {

    private EntityManagerFactory emf;

    public VActividadDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public List<VTipoActividad> listoTipoActividades() {
        List<VTipoActividad> listaActividades = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VTipoActividad v WHERE v.estado = :estado";
        Query query = em.createQuery(sql);
        query.setParameter("estado", true);
        listaActividades = query.getResultList();
        em.close();
        return listaActividades;
    }

    public VTipoActividad buscarTipoActividades(int idTipoActividad) {
        VTipoActividad actividad = new VTipoActividad();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VTipoActividad v WHERE v.vTipoActividadId = :idTipoActividad";
        Query query = em.createQuery(sql);
        query.setParameter("idTipoActividad", idTipoActividad);
        actividad = (VTipoActividad) query.getSingleResult();
        em.close();
        return actividad;
    }

    public Boolean nuevaActividad(VActividad a) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(a);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            System.out.println("Aqui" + e);
            resp = false;
        }
        return resp;
    }

    public List<VActividad> listaActividadesSiguientes(int idVendedor) {
        List<VActividad> listaActividades = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "select act.* "
                + " from v_actividad act "
                + " where act.estado= ?1 "
                + " and act.v_vendedor_id= ?2 "
                + " and act.etapa in ('AC','IN','FN') "
                + " and date(act.ffin) >= date(now())";
        Query query = em.createNativeQuery(sql, VActividad.class);
        query.setParameter(1, true);
        query.setParameter(2, idVendedor);
        listaActividades = query.getResultList();
        em.close();
        return listaActividades;
    }

    public VActividad buscarActividadPorId(int idActividad) {
        VActividad actividad = new VActividad();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VActividad v WHERE v.vActividadId = :vActividadId";
        Query query = em.createQuery(sql);
        query.setParameter("vActividadId", idActividad);
        actividad = (VActividad) query.getSingleResult();
        em.close();
        return actividad;
    }

    public Boolean actualizarActividad(VActividad a) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(a);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean nuevoRegistroActividad(VRegistroActividad a) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(a);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public VRegistroActividad buscarRegistroActividad(int idActividad) {
        VRegistroActividad actividad = new VRegistroActividad();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VRegistroActividad v WHERE v.vActividadId.vActividadId=:vActividadId and v.estado = :estado";
        Query query = em.createQuery(sql);
        query.setParameter("vActividadId", idActividad);
        query.setParameter("estado", true);
        actividad = (VRegistroActividad) query.getSingleResult();
        em.close();
        return actividad;
    }

    public Boolean actualizarRegistroActividad(VRegistroActividad a) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(a);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public List<VActividad> listaActividadesVendedor(int idVendedor, String desde, String hasta) {
        List<VActividad> listaActividades = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "select act.* "
                + " from v_actividad act "
                + " where act.estado= ?1 "
                + " and act.v_vendedor_id= ?2 "
                + " and act.etapa in ('AC','IN','FN') "
                + " and date(act.finicio) >= date(?3)"
                + " and date(act.ffin) <= date(?4)"
                + " order by act.finicio asc";
        Query query = em.createNativeQuery(sql, VActividad.class);
        query.setParameter(1, true);
        query.setParameter(2, idVendedor);
        query.setParameter(3, desde);
        query.setParameter(4, hasta);
        listaActividades = query.getResultList();
        em.close();
        return listaActividades;
    }

    public List<Object[]> diasConsulta(int idVendedor, String inicio, String fin) {
        List<Object[]> diasConActividad = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "select distinct(date(finicio)) as fecha,  "
                + "	extract(dow from finicio) as dia  "
                + " from v_actividad  "
                + " where v_vendedor_id = ?1 "
                + " and date(finicio) >= date(?2) "
                + " and date(ffin) <= date(?3) "
                + " and etapa not in ('AN')"
                + " order by 1 asc";
        Query query = em.createNativeQuery(sql);
        query.setParameter(1, idVendedor);
        query.setParameter(2, inicio);
        query.setParameter(3, fin);
        diasConActividad = query.getResultList();
        em.close();
        return diasConActividad;
    }

    public Boolean nuevoVariedadActividad(VVariedadActividad v) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(v);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public List<VVariedadActividad> variedadesPorActividad(int idActividad) {
        List<VVariedadActividad> lista = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VVariedadActividad v WHERE v.vActividadId.vActividadId = :idactividad and v.estado=true ";
        Query query = em.createQuery(sql);
        query.setParameter("idactividad", idActividad);
        lista = query.getResultList();
        em.close();
        return lista;
    }

    public Boolean eliminarVariedadActividad(VVariedadActividad v) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(v);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public VVariedadActividad variedadPorActividad(int idActividad) {
        VVariedadActividad v = new VVariedadActividad();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VVariedadActividad v WHERE v.vVariedadActividadId = :idactividad";
        Query query = em.createQuery(sql);
        query.setParameter("idactividad", idActividad);
        v = (VVariedadActividad) query.getSingleResult();
        em.close();
        return v;
    }
}
