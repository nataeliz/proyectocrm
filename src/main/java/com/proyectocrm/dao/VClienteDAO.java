/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VActividad;
import com.proyectocrm.entity.VCliente;
import com.proyectocrm.entity.VClienteVendedor;
import com.proyectocrm.entity.VEmpleado;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author user
 */
public class VClienteDAO {

    private EntityManagerFactory emf;

    public VClienteDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public List<VClienteVendedor> buscarClientesPorVendedor(int idVendedor) {
        List<VClienteVendedor> clientes = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VClienteVendedor u "
                + " WHERE u.vVendedorId.vVendedorId=:idVendedor "
                + " and u.vClienteId.estado=true";
        Query query = em.createQuery(sql);
        query.setParameter("idVendedor", idVendedor);
        clientes = query.getResultList();
        return clientes;
    }

    public List<VClienteVendedor> buscarTodosClientes() {
        List<VClienteVendedor> clientes = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VClienteVendedor u WHERE u.vClienteId.estado=true";
        Query query = em.createQuery(sql);
        clientes = query.getResultList();
        return clientes;
    }

    public List<VCliente> buscarClientesPorNombre(String nombre) {
        List<VCliente> clientes = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        String sql = "select c.* "
                + "from v_cliente c  "
                + "where c.nombre like upper('%" + nombre + "%') and c.estado=true";
        Query query = em.createNativeQuery(sql, VCliente.class);
        //query.setParameter(1, nombre);
        clientes = query.getResultList();
        return clientes;
    }

    public List<VCliente> todosClientesAdministrador(boolean estado) {
        List<VCliente> clientes = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT u FROM VCliente u WHERE u.estado=:estado ORDER BY u.nombre ASC";
        Query query = em.createQuery(sql);
        query.setParameter("estado", estado);
        clientes = query.getResultList();
        return clientes;
    }

    public VClienteVendedor infoClienteVendedor(int idCliente) {
        VClienteVendedor c = new VClienteVendedor();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VClienteVendedor v WHERE v.vClienteId.vClienteId = :vClienteId";
        Query query = em.createQuery(sql);
        query.setParameter("vClienteId", idCliente);
        c = (VClienteVendedor) query.getSingleResult();
        return c;
    }

    public VCliente infoClientePorId(int idCliente) {
        VCliente c = new VCliente();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VCliente v WHERE v.vClienteId = :vClienteId";
        Query query = em.createQuery(sql);
        query.setParameter("vClienteId", idCliente);
        c = (VCliente) query.getSingleResult();
        return c;
    }

    public Boolean nuevoCliente(VCliente c) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(c);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean nuevoClienteVendedor(VClienteVendedor vcv) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(vcv);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean actualizaClienteVendedor(VClienteVendedor vcv) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(vcv);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean actualizaCliente(VCliente c) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(c);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public List<VCliente> listaClientes() {
        List<VCliente> listaClientes = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VCliente v";
        Query query = em.createQuery(sql);
        listaClientes = query.getResultList();
        em.close();
        return listaClientes;
    }

}
