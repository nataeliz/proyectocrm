/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.dao;

import com.proyectocrm.entity.VBreeder;
import com.proyectocrm.entity.VBreederVariedad;
import com.proyectocrm.entity.VImagen;
import com.proyectocrm.entity.VVariedad;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author user
 */
public class VVariedadDAO {

    private EntityManagerFactory emf;

    public VVariedadDAO() {
        emf = Persistence.createEntityManagerFactory("crmPU");
    }

    public List<VVariedad> listaVariedades() {
        List<VVariedad> listaVariedades = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VVariedad v WHERE v.estado = :estado";
        Query query = em.createQuery(sql);
        query.setParameter("estado", true);
        listaVariedades = query.getResultList();
        em.close();
        return listaVariedades;
    }

    public List<VVariedad> buscaVariedades(String nombre) {
        List<VVariedad> listaVariedades = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "select v.* "
                + "from v_variedad v  "
                + "where v.nombre like upper('%" + nombre + "%') and v.estado=true";
        Query query = em.createNativeQuery(sql, VVariedad.class);
        listaVariedades = query.getResultList();
        em.close();
        return listaVariedades;
    }

    public VBreederVariedad listaVariedadesBreeder(int idVariedad) {
        VBreederVariedad variedadBreeder = new VBreederVariedad();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VBreederVariedad v WHERE v.vVariedadId.vVariedadId=:idVariedad";
        Query query = em.createQuery(sql);
        query.setParameter("idVariedad", idVariedad);
        variedadBreeder = (VBreederVariedad) query.getSingleResult();
        em.close();
        return variedadBreeder;
    }

    public List<VImagen> listarImagenes(int idVariedad) {
        List<VImagen> listaImagenes = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VImagen v WHERE v.estado = true and v.vVariedadId.vVariedadId=:idVariedad";
        Query query = em.createQuery(sql);
        query.setParameter("idVariedad", idVariedad);
        listaImagenes = query.getResultList();
        em.close();
        return listaImagenes;
    }

    public Boolean nuevoRegistroImagen(VImagen img) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(img);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public List<VBreeder> listaBreeders() {
        List<VBreeder> listabDeBreeders = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VBreeder v WHERE v.estado = :estado";
        Query query = em.createQuery(sql);
        query.setParameter("estado", true);
        listabDeBreeders = query.getResultList();
        em.close();
        return listabDeBreeders;
    }

    public Boolean nuevaVariedad(VVariedad var) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(var);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean nuevaVariedadBreeder(VBreederVariedad var) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(var);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public VBreeder buscarBreeder(int idBreeder) {
        VBreeder breeder = new VBreeder();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VBreeder v WHERE v.vBreederId=:idBreeder";
        Query query = em.createQuery(sql);
        query.setParameter("idBreeder", idBreeder);
        breeder = (VBreeder) query.getSingleResult();
        em.close();
        return breeder;
    }

    public VVariedad buscarVariedad(int idVariedad) {
        VVariedad variedad = new VVariedad();
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT v FROM VVariedad v WHERE v.vVariedadId=:idVariedad";
        Query query = em.createQuery(sql);
        query.setParameter("idVariedad", idVariedad);
        variedad = (VVariedad) query.getSingleResult();
        em.close();
        return variedad;
    }

    public Boolean actualizaVariedad(VVariedad var) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(var);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }

    public Boolean actualizaBreederVariedad(VBreederVariedad bv) {
        boolean resp = false;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(bv);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        return resp;
    }
}
