/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VRolDAO;
import com.proyectocrm.entity.VRol;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Desarrollador 2
 */
@Named(value = "rolControlador")
@ViewScoped
public class rolControlador implements Serializable {

    private List<VRol> listaRolesCbx;
    private List<VRol> listaRoles;
    private VRol rol;

    private List<SelectItem> listRoles;

    private rolControlador() {
        super();
    }

    @PostConstruct
    protected void init() {
    }

    public List<VRol> getListaRoles() {
        VRolDAO eDao = new VRolDAO();
        this.listaRoles = eDao.listarRoles();
        return listaRoles;
    }

    public void setListaRoles(List<VRol> listaRoles) {
        this.listaRoles = listaRoles;
    }

    public String getEstado(VRol rol) {
        String estado = null;
        if (rol.getEstado() == true) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    public List<VRol> getListaRolesCbx() {
        VRolDAO eDao = new VRolDAO();
        this.listaRolesCbx = eDao.listarRolesCbx(true);
        return listaRolesCbx;
    }

    public void setListaRolesCbx(List<VRol> listaRolesCbx) {
        this.listaRolesCbx = listaRolesCbx;
    }

    public VRol getRol() {
        return rol;
    }

    public void setRol(VRol rol) {
        this.rol = rol;
    }

    public List<SelectItem> getListRoles() {
        this.listRoles = new ArrayList<SelectItem>();
        listRoles.clear();
        for (VRol r : getListaRolesCbx()) {
            SelectItem items = new SelectItem(r.getVRolId(), r.getNombre());
            listRoles.add(items);
        }
        return listRoles;
    }

    public void setListRoles(List<SelectItem> listRoles) {
        this.listRoles = listRoles;
    }

}
