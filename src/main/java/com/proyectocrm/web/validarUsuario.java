/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VEmpleadoDAO;
import com.proyectocrm.entity.VEmpleado;
import com.proyectocrm.utils.FacesUtil;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Desarrollador 2
 */
@ManagedBean
@SessionScoped
public class validarUsuario implements Serializable {

    private VEmpleado VEmpleado;

    private String usuario;
    private String password;
    private String nombre;

    public validarUsuario() {
        this.VEmpleado = new VEmpleado();
    }

    public void login() throws IOException {
        boolean loggedIn = false;

        VEmpleadoDAO eDao = new VEmpleadoDAO();
        VEmpleado v = new VEmpleado();
        v = eDao.validarUsuario(this.VEmpleado.getUsuario(), this.VEmpleado.getPassword());
        String ruta = "/ProyectoCRM/ventas/inicio.xhtml";

        if (v != null) {
            loggedIn = true;
            FacesContext.getCurrentInstance().getExternalContext().redirect(ruta);
        } else {
            loggedIn = false;
            FacesUtil.mensajeWarnDialog("Usuario o contraseña incorrecta");
        }
    }

    public VEmpleado getVEmpleado() {
        return VEmpleado;
    }

    public void setVEmpleado(VEmpleado VEmpleado) {
        this.VEmpleado = VEmpleado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
