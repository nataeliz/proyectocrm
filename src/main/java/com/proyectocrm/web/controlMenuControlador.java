/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VMenuDAO;
import com.proyectocrm.entity.VMenu;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Desarrollador 2
 */
@Named
@SessionScoped
public class controlMenuControlador implements Serializable {

    private List<VMenu> lista;
    private MenuModel model;

    @PostConstruct
    public void init() {
        this.lista = new ArrayList<>();
        VMenuDAO eDAO = new VMenuDAO();
        this.lista = eDAO.listaMenusEstado(true);
        establecerPermisos();
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public void establecerPermisos() {
        try {
            model = new DefaultMenuModel();
            int submenu, dato;

            for (VMenu m : lista) {
                if (m.getTipo().equals("S")) {
                    DefaultSubMenu firstSubmenu = new DefaultSubMenu(m.getVVentanaId().getNombre());
                    //firstSubmenu.setIcon("home");

                    submenu = m.getVMenuId();
                    for (VMenu i : lista) {
                        if (i.getCodigoSubmenu() != null) {

                            dato = Integer.parseInt(i.getCodigoSubmenu().getVMenuId().toString());
                            if (submenu == dato) {  //Lista de items de un submenu
                                DefaultMenuItem item = new DefaultMenuItem(i.getVVentanaId().getNombre());
                                item.setUrl(i.getUrl());
                                item.setStyleClass("menu");
                                //item.setIcon("close");
                                firstSubmenu.addElement(item);
                            }
                        }
                    }
                    model.addElement(firstSubmenu);
                } else {
                    if (m.getCodigoSubmenu() == null) {
                        DefaultMenuItem item = new DefaultMenuItem(m.getVVentanaId().getNombre());
                        item.setUrl(m.getUrl());
                        model.addElement(item);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

}
