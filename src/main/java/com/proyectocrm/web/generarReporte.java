/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VEmpleadoDAO;
import com.proyectocrm.entity.VVendedor;
import com.proyectocrm.utils.JasperReportUtils;
import com.proyectocrm.utils.FacesUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author user
 */
@Named(value = "generarReporte")
@ViewScoped
public class generarReporte implements Serializable {

    private static final long serialVersionUID = -1091103729705136449L;
    private static final Logger LOG = Logger.getLogger(generarReporte.class.getName());

    private Date fechaInicio;
    private Date fechaFin;
    private List<VVendedor> vendedores;
    private int idVendedor;

    private generarReporte() {
        super();
    }

    @PostConstruct
    protected void init() {
        this.vendedores = new ArrayList<>();
        VEmpleadoDAO eDAO = new VEmpleadoDAO();
        this.vendedores = eDAO.BuscarVendedores();
        System.out.println(this.vendedores);
        this.idVendedor = 0;
    }

    /**
     * Selecciona la fecha del calendario INICIO.
     *
     * @param event Evento
     */
    public void onChangeFechaI(ValueChangeEvent event) {
        this.fechaInicio = (Date) event.getNewValue();
    }

    /**
     * Selecciona la fecha del calendario INICIO.
     *
     * @param event Evento
     */
    public void onSelectFechaI(SelectEvent event) {
        this.fechaInicio = (Date) event.getObject();
    }

    /**
     * Selecciona la fecha del calendario INICIO.
     *
     * @param event Evento
     */
    public void onChangeFechaF(ValueChangeEvent event) {
        this.fechaFin = (Date) event.getNewValue();
    }

    /**
     * Selecciona la fecha del calendario INICIO.
     *
     * @param event Evento
     */
    public void onSelectFechaF(SelectEvent event) {
        this.fechaFin = (Date) event.getObject();
    }

    public void imprimirReporte() {
        try {
            if (this.fechaInicio == null || this.fechaFin == null || this.idVendedor == 0) {
                FacesUtil.mensajeWarnDialog("DEBE SELECCIONAR EL RANGO DE FECHAS");
            } else {
                
                VEmpleadoDAO eDAO = new VEmpleadoDAO();
                VVendedor v= new VVendedor();
                v=eDAO.vendedorId(idVendedor);
                

                Map<String, Object> prm = new HashMap<>();
                prm.put("inicio", FacesUtil.obtenerSinHoraFecha(this.fechaInicio));
                prm.put("fin", FacesUtil.obtenerSinHoraFecha(this.fechaFin));
                prm.put("idvendedor", this.idVendedor);           
                prm.put("vendedor", v.getVEmpleadoId().getNombre());
                prm.put("pathsubreport", JasperReportUtils.PATH_SUB_REPORTE);

                JasperReportUtils jru = new JasperReportUtils();
                try {
                    jru.generarReporteBD(JasperReportUtils.PATH_ACTIVIDADES,
                            JasperReportUtils.TIPO_XLS, "actividades", prm);
                } catch (Exception e) {
                    LOG.log(Level.SEVERE, "No se puede crear la resolución.", e);
                }
            }

        } catch (Exception e) {
            System.out.println("Error: "+ e.getMessage());
        }

    }
public void imprimirEjecutado() {
        try {
            if (this.fechaInicio == null || this.fechaFin == null || this.idVendedor == 0) {
                FacesUtil.mensajeWarnDialog("DEBE SELECCIONAR EL RANGO DE FECHAS");
            } else {
                
                VEmpleadoDAO eDAO = new VEmpleadoDAO();
                VVendedor v= new VVendedor();
                v=eDAO.vendedorId(idVendedor);
                

                Map<String, Object> prm = new HashMap<>();
                prm.put("inicio", FacesUtil.obtenerSinHoraFecha(this.fechaInicio));
                prm.put("fin", FacesUtil.obtenerSinHoraFecha(this.fechaFin));
                prm.put("idvendedor", this.idVendedor);           
                prm.put("vendedor", v.getVEmpleadoId().getNombre());
                prm.put("pathsubreport", JasperReportUtils.PATH_SUB_REPORTE);

                JasperReportUtils jru = new JasperReportUtils();
                try {
                    jru.generarReporteBD(JasperReportUtils.PATH_ACTIVIDADES_EJECUTADO,
                            JasperReportUtils.TIPO_XLS, "actividades", prm);
                } catch (Exception e) {
                    LOG.log(Level.SEVERE, "No se puede crear la resolución.", e);
                }
            }

        } catch (Exception e) {
            System.out.println("Error: "+ e.getMessage());
        }
    }
    public void imprimirDetallado() {
        try {
            if (this.fechaInicio == null || this.fechaFin == null || this.idVendedor == 0) {
                FacesUtil.mensajeWarnDialog("DEBE SELECCIONAR EL RANGO DE FECHAS");
            } else {
                
                VEmpleadoDAO eDAO = new VEmpleadoDAO();
                VVendedor v= new VVendedor();
                v=eDAO.vendedorId(idVendedor);
                

                Map<String, Object> prm = new HashMap<>();
                prm.put("inicio", FacesUtil.obtenerSinHoraFecha(this.fechaInicio));
                prm.put("fin", FacesUtil.obtenerSinHoraFecha(this.fechaFin));
                prm.put("idvendedor", this.idVendedor);           
                prm.put("vendedor", v.getVEmpleadoId().getNombre());

                JasperReportUtils jru = new JasperReportUtils();
                try {
                    jru.generarReporteBD(JasperReportUtils.PATH_ACTIVIDADES_DETALLADO,
                            JasperReportUtils.TIPO_HTML, "actividades", prm);
                } catch (Exception e) {
                    LOG.log(Level.SEVERE, "No se puede crear la resolución.", e);
                }
            }

        } catch (Exception e) {
            System.out.println("Error: "+ e.getMessage());
        }

    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<VVendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(List<VVendedor> vendedores) {
        this.vendedores = vendedores;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

}
