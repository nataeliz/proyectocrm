/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VEmpleadoDAO;
import com.proyectocrm.entity.VEmpleado;
import com.proyectocrm.entity.VRol;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Desarrollador 2
 */
@Named(value = "empleadoControlador")
@ViewScoped
public class empleadoControlador implements Serializable {

    private List<VEmpleado> listaEmpleados;
    private VEmpleado empleado;
    private int rolId;

    private empleadoControlador() {
        super();
    }

    @PostConstruct
    protected void init() {
    }

    public List<VEmpleado> getListaEmpleados() {
        VEmpleadoDAO eDao = new VEmpleadoDAO();
        this.listaEmpleados = eDao.listaEmpleados();
        return listaEmpleados;
    }

    public void setListaEmpleados(List<VEmpleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    public String getEstado(boolean est) {
        String estado = null;
        if (est == true) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    public VEmpleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(VEmpleado empleado) {
        this.empleado = empleado;
    }

    public void nuevoEmpleado() {
        empleado = new VEmpleado();
    }

    public int getRolId() {
        rolId = 0;
        if (empleado != null) {
            if (empleado.getVRolId() != null) {
                rolId = empleado.getVRolId().getVRolId();
            }
        }
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public void crearEmpleado() {
        try {
            VRol objRol = new VRol();
            objRol.setVRolId(rolId);
            empleado.setVRolId(objRol);

            VEmpleadoDAO eDao = new VEmpleadoDAO();
            eDao.nuevoEmpleado(empleado);
            if (empleado.getVEmpleadoId() != 0) {
                //idcolor = .empleado.getVEmpleadoId();
                //facesUtils.addInfoMessageNew();
            } else {
                //facesUtils.addErrorMessageNew();
            }
        } catch (Exception e) {
            //facesUtils.addErrorMessageNew();
        }
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('dialogNuevo').hide();");
    }

    public void editarEmpleado() {
        try {
            VRol objRol = new VRol();
            objRol.setVRolId(rolId);
            empleado.setVRolId(objRol);

            VEmpleadoDAO eDao = new VEmpleadoDAO();
            eDao.actualizaEmpleado(empleado);
            if (empleado.getVEmpleadoId() != 0) {
                //idcolor = .empleado.getVEmpleadoId();
                //facesUtils.addInfoMessageNew();
            } else {
                //facesUtils.addErrorMessageNew();
            }
        } catch (Exception e) {
            //facesUtils.addErrorMessageNew();
        }
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('dialogEditar').hide();");
    }

    public void eliminarEmpleado() {
        try {
            VEmpleadoDAO eDao = new VEmpleadoDAO();
            eDao.eliminaEmpleado(empleado);
            //facesUtils.addInfoMessageDel();
        } catch (Exception e) {
            //facesUtils.addErrorMessageDel();
        }
    }
}
