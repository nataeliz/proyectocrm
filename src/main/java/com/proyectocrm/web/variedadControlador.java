/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VVariedadDAO;
import com.proyectocrm.entity.VImagen;
import com.proyectocrm.entity.VVariedad;
import com.proyectocrm.utils.DirectorioImagen;
import com.proyectocrm.utils.FacesUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Desarrollador 2
 */
@Named(value = "variedadControlador")
@ViewScoped
public class variedadControlador implements Serializable {

    private List<VVariedad> listaVariedades;
    private VVariedad variedad;

    private UploadedFile file;
    private VImagen imagen;
    private String destino;

    private UploadedFile filep;

    private variedadControlador() {
        super();
    }

    @PostConstruct
    protected void init() {
    }

    public List<VVariedad> getListaVariedades() {
        VVariedadDAO cDao = new VVariedadDAO();
        this.listaVariedades = cDao.listaVariedades();
        return listaVariedades;
    }

    public void setListaVariedades(List<VVariedad> listaVariedades) {
        this.listaVariedades = listaVariedades;
    }

    public String getEstado(boolean est) {
        String estado = null;
        if (est == true) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    public VVariedad getVariedad() {
        return variedad;
    }

    public void setVariedad(VVariedad variedad) {
        this.variedad = variedad;
    }

    public void nuevaVariedad() {
        variedad = new VVariedad();
    }

    //Cargar imagen
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            file = event.getFile();
            nuevaImagen();
            FacesUtil.addCargarImagen();
        } catch (Exception e) {
        }
    }

    public void nuevaImagen() {
        if (upload1()) {
            imagen.setRutaImagen(DirectorioImagen.SEPARADOR + destino);
            destino = "";
        }
    }

    public Boolean upload1() {
        java.util.Date fecha = new Date();
        try {
            if (!(getFile().getFileName()).equals("")) { //verifica si se ingreso un archivo para de esa forma crear el directorio
                if (variedad.getVVariedadId() != 0) {
                    destino = DirectorioImagen.URL + variedad.getVVariedadId() + fecha;
                }
                System.out.println("destino= " + destino);
                File folder = new File(FacesUtil.getPath() + destino);

                if (!folder.exists()) {
                    folder.mkdirs();
                }

                copyFile(getFile().getFileName(), getFile().getInputstream());
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void copyFile(String fileName, InputStream in) {
        try {
            destino = destino + DirectorioImagen.SEPARADOR + fileName;
            System.out.println("destino= " + destino);
            OutputStream out = new FileOutputStream(new File(FacesUtil.getPath() + destino));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();
        } catch (Exception e) {
        }
    }

    public String getRutaImagen() {
        String rutaImagen = null;
        if (imagen.getRutaImagen() != null) {
            rutaImagen = imagen.getRutaImagen();
        } else {
            rutaImagen = "/resources/photos/photo_default.png";  //Agregar una foto por defecto
        }
        return rutaImagen;
    }

    //Fin cargar imagen
    public void crearVariedad() {

    }

    public void editarVariedad() {

    }

    public void eliminarVariedad() {

    }

    ///
    public UploadedFile getFilep() {
        return filep;
    }

    public void setFilep(UploadedFile filep) {
        this.filep = filep;
    }
    
    public void upload() {
      if (filep != null) {
          try {
              ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance();
              //String path = servletContext.getRealPath(destino)
          } catch (Exception e) {
          }
      }
    }

}
