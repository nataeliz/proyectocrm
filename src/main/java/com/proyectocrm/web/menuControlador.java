/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VMenuDAO;
import com.proyectocrm.dao.VVentanaDAO;
import com.proyectocrm.entity.VMenu;
import com.proyectocrm.entity.VRol;
import com.proyectocrm.entity.VVentana;
import com.proyectocrm.utils.FacesUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Desarrollador 2
 */
@Named(value = "menuControlador")
@ViewScoped
public class menuControlador implements Serializable {

    private List<VMenu> listaMenus;
    private VMenu menu;
    private int rolId;
    private int submenuId;
    private int ventanaId;
    private List<VMenu> listaMenusCbx;
    private List<SelectItem> listMenus;
    private List<VVentana> listaVentanasCbx;
    private List<SelectItem> listVentanas;

    private menuControlador() {
        super();
    }

    @PostConstruct
    protected void init() {
    }

    public List<VMenu> getListaMenus() {
        VMenuDAO mDao = new VMenuDAO();
        this.listaMenus = mDao.listaMenus();
        return listaMenus;
    }

    public void setListaMenus(List<VMenu> listaMenus) {
        this.listaMenus = listaMenus;
    }

    public VMenu getMenu() {
        return menu;
    }

    public void setMenu(VMenu menu) {
        this.menu = menu;
    }

    public void nuevoMenu() {
        menu = new VMenu();
    }

    public String getEstado(boolean est) {
        String estado = null;
        if (est == true) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    public String getSubmenu(int subMenu) {
        String nombreMenu = null;
        for (VMenu m : listaMenus) {
            if (m.getVMenuId() == subMenu) {
                nombreMenu = m.getVVentanaId().getNombre();
            }
        }
        return nombreMenu;
    }

    public String getTipo(String ti) {
        String tipo = null;
        if (ti.equals("S")) {
            tipo = "Submenu";
        } else {
            tipo = "Item";
        }
        return tipo;
    }

    public int getRolId() {
        rolId = 0;
        if (menu != null) {
            if (menu.getVRolId() != null) {
                rolId = menu.getVRolId().getVRolId();
            }
        }
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public List<VMenu> getListaMenusCbx() {
        VMenuDAO eDao = new VMenuDAO();
        this.listaMenusCbx = eDao.listaMenusEstado(true);
        return listaMenusCbx;
    }

    public void setListaMenusCbx(List<VMenu> listaMenusCbx) {
        this.listaMenusCbx = listaMenusCbx;
    }

    public List<SelectItem> getListMenus() {
        this.listMenus = new ArrayList<SelectItem>();
        listMenus.clear();
        for (VMenu m : getListaMenusCbx()) {
            SelectItem items = new SelectItem(m.getVMenuId(), m.getVVentanaId().getNombre());
            listMenus.add(items);
        }
        return listMenus;
    }

    public void setListMenus(List<SelectItem> listMenus) {
        this.listMenus = listMenus;
    }

    public int getSubmenuId() {
        submenuId = 0;
        if (menu != null) {
            if (menu.getCodigoSubmenu() != null) {
                submenuId = menu.getCodigoSubmenu().getVMenuId();
            }
        }
        return submenuId;
    }

    public void setSubmenuId(int submenuId) {
        this.submenuId = submenuId;
    }

    public int getVentanaId() {
        ventanaId = 0;
        if (menu != null) {
            if (menu.getVVentanaId() != null) {
                ventanaId = menu.getVVentanaId().getVVentanaId();
            }
        }
        return ventanaId;
    }

    public void setVentanaId(int ventanaId) {
        this.ventanaId = ventanaId;
    }

    public List<VVentana> getListaVentanasCbx() {
        VVentanaDAO eDao = new VVentanaDAO();
        this.listaVentanasCbx = eDao.listarVentanasCbx(true);
        return listaVentanasCbx;
    }

    public void setListaVentanasCbx(List<VVentana> listaVentanasCbx) {
        this.listaVentanasCbx = listaVentanasCbx;
    }

    public List<SelectItem> getListVentanas() {
        this.listVentanas = new ArrayList<SelectItem>();
        listVentanas.clear();
        for (VVentana v : getListaVentanasCbx()) {
            SelectItem items = new SelectItem(v.getVVentanaId(), v.getNombre());
            listVentanas.add(items);
        }
        return listVentanas;
    }

    public void setListVentanas(List<SelectItem> listVentanas) {
        this.listVentanas = listVentanas;
    }

    public void crearMenu() {
        try {
            VRol objRol = new VRol();
            objRol.setVRolId(rolId);
            menu.setVRolId(objRol);

            VMenu objMenu = new VMenu();
            objMenu.setVMenuId(submenuId);
            menu.setCodigoSubmenu(objMenu);

            VVentana objVentana = new VVentana();
            objVentana.setVVentanaId(ventanaId);
            menu.setVVentanaId(objVentana);

            if (menu.getTipo().equals("S")) { //cuando es un submenu no e3s necesario el codigo del submenu ni la url
                menu.setCodigoSubmenu(null);
                menu.setUrl(null);
            }

            VMenuDAO eDao = new VMenuDAO();
            eDao.nuevoMenu(menu);
            if (menu.getVMenuId() != 0) {
                //idcolor = .empleado.getVEmpleadoId();
                //facesUtils.addInfoMessageNew();
            } else {
                //facesUtils.addErrorMessageNew();
            }
        } catch (Exception e) {
            //facesUtils.addErrorMessageNew();
        }
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('dialogNuevo').hide();");
    }

    public void editarMenu() {
        boolean resp = false;
        try {
            VRol objRol = new VRol();
            objRol.setVRolId(rolId);
            menu.setVRolId(objRol);

            VMenu objMenu = new VMenu();
            objMenu.setVMenuId(submenuId);
            menu.setCodigoSubmenu(objMenu);

            VVentana objVentana = new VVentana();
            objVentana.setVVentanaId(ventanaId);
            menu.setVVentanaId(objVentana);

            if (menu.getTipo().equals("S")) { //cuando es un submenu no e3s necesario el codigo del submenu ni la url
                menu.setCodigoSubmenu(null);
                menu.setUrl(null);
            }

            VMenuDAO eDao = new VMenuDAO();
            resp = eDao.actualizaMenu(menu);
            if (resp) {
                //idcolor = .empleado.getVEmpleadoId();
                FacesUtil.addInfoMessageEdit();
            } else {
                FacesUtil.addErrorMessageEdit();
            }
        } catch (Exception e) {
            FacesUtil.addErrorMessageEdit();
        }
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('dialogEditar').hide();");
    }

    public void eliminarMenu() {
        try {
            VMenuDAO eDao = new VMenuDAO();
            eDao.eliminaMenu(menu);
            //facesUtils.addInfoMessageDel();
        } catch (Exception e) {
            //facesUtils.addErrorMessageDel();
        }
    }

}
