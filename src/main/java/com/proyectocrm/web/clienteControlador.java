/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectocrm.web;

import com.proyectocrm.dao.VClienteDAO;
import com.proyectocrm.entity.VCliente;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Desarrollador 2
 */
@Named(value = "clienteControlador")
@ViewScoped
public class clienteControlador implements Serializable {

    private List<VCliente> listaClientes;

    private clienteControlador() {
        super();
    }

    @PostConstruct
    protected void init() {
    }

    public List<VCliente> getListaClientes() {
        VClienteDAO cDao = new VClienteDAO();
        this.listaClientes = cDao.listaClientes();
        return listaClientes;
    }

    public void setListaClientes(List<VCliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public String getEstado(boolean est) {
        String estado = null;
        if (est == true) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    public String getEsNacional(VCliente cliente) {
        String nacional = null;
        if (cliente.getEstado() == true) {
            nacional = "Si";
        } else {
            nacional = "No";
        }
        return nacional;
    }

}
